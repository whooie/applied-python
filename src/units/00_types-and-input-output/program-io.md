# Program input/output

So far, we've been dealing entirely with what can happen inside a program. On
this page, we'll introduce a couple simple ways to get information from and send
information to your program.

## Contents
<!-- toc -->

## Program input
The central goal surrounding input is to have the user be able to alter the
exact behavior of the program based on user-generated information without having
to change the text of the program itself. The simple method taught to most new
Python users for receiving user-generated input is the aptly named `input()`
*function*. We'll talk more about functions in the next unit, but for now we'll
take a simple look at how to interact with functions by drawing analogy to their
mathematical counterparts.

In math, a function usually (non-rigorously) defines an operation performed on a
set of inputs to produce an output. For instance, we can abstractly define a
function $f$ of three variables (inputs) $x$, $y$, and $z$:
$$
f(x, y, z)
$$
In this example, $f$ acts as a sort of name for the function, and all its inputs
are enclosed by parentheses that immediately follow this name, separated by
commas.

In Python (and indeed many other programming languages), functions are used in a
somewhat superficially similar manner: each function has a name, and in order to
*call* or *invoke* a function, we write the function's name, followed by its
inputs surrounded by parentheses and separated by commas:
```Python
my_function(arg1, arg2, arg3)
```
One difference that is immediately recognizable is that the names of functions
and quantities are not limited to one letter (in principle this is allowed in
math too, but not generally done in practice). Another that is perhaps a bit
subtle is that in programming, the notion of a function is generalized to have
looser requirements on the kinds of output that the set of operations can
produce: While in math, we usually deal with functions that produce outputs of
generally the same kind of thing as their inputs (e.g. a function of three real
numbers outputs another number), functions in programming commonly product
outputs of entirely different kinds. For example, a (programming) function may
take three numbers and instead write the numbers to a file, or take a list and
mutate it in place, in which case it doesn't really even have a concrete output.
(How would you describe the outputs of these functions in a rigorous manner?)
Given this additional freedom, it's usually more useful to think of functions in
terms of their operations than their outputs, although of course we still think
of both.

With this out of the way, let's talk more about the `input()` function. This is
a good opportunity to show off one of Python's great features for learners:
`help`. `help` is another function that we can use to find information on other
Python objects, including functions. For example, we can call it on the `input`
function (i.e. `help(input)`) that we'd like to learn about. When we do, we'll
see this information:
```
Help on built-in function input on module builtins:

input(prompt=None, /)
    Read a string from standard input.  The trailing newline is stripped.

    The prompt string, if given, is printed to standard output without a
    trailing newline before reading input.

    If the user hits EOF (*nix: Ctrl-D, Windows: Ctrl-Z+Return), raise EOFError.
    On *nix systems, readline is used if available.
```
Here, we can glean a number of important pieces of information. The first line
tells us the `input()` is a "built-in" function, which essentially means that
it's available to every Python program that can be written -- this is not
necessarily the case if your program requires third-party software that must be
downloaded from another source.

On the next non-empty line, we can see what is called the *signature* of
`input()`, which is the name of the function and the series of arguments it
expects. In this case, the comma-separated items in the parentheses are:
- `prompt=None`: This item defines a function argument called `prompt`, which is
  set equal to a value `None`. This assignment indicates that `prompt` is an
  *optional* argument that can be given some value other than `None` (or not).
  We'll discuss this feature in more detail in the next unit.
- `/`: We can disregard this as mostly unimportant at this point (again, to be
  discussed in the next unit), but essentially it indicates the end of all of
  the arguments expected by `input()`.

Finally, the remaining lines describe the overall behavior of the function:
`input()` is used to read user input while the program is running, and return
whatever the input is as a `str`. The second of these paragraphs describes what
the `prompt` argument controls, while the third offers some technical notes on
behavior that depends on the content of the user input.

When `input("my prompt: ")` is called from a program, the program's execution is
momentarily halted to allow the user to type whatever they want until they hit
the RETURN/ENTER key. Everything typed up to just before the RETURN/ENTER key
will then be *returned* (output) from the function as a `str` (which can then be
stored in a variable). The `prompt` argument is a `str` that is printed to the
terminal just before the space where the user can type, and is commonly used to
convey some information to the user about what kind of input the program
expects.

Consider the following example:
```Python
user_input = input(">> ")
```
When executed, the program will print this out:
```
>> _
```
where the `_` indicates the placement of the cursor. If you were the one running
this (which you are strongly invited to try), you could then type e.g. `blah
blah blah<RETURN>`, and then the program would continue on with the variable
`user_input` then having the value `"blah blah blah"`.

---
<details>
<summary><strong>Check your understanding</strong></summary>

One common use case for accepting user input in this way is to record a series
of input `str`s so they can be referred to later in a program. This sounds like
a good opportunity to use a `list` to store them! Write a small program that
initializes an empty `list`, then asks for user input three times, appending the
input to the `list` each time.

<details>
<summary>Answer</summary>

Your program should look something like this:
```Python
# initialize the list
user_inputs = list() # `user_inputs = []` would also work just as well

# set the prompt for each input opportunity so it can be easily changed if
# needed
prompt = "type something: "

# append user input to `user_inputs` three times
user_inputs.append(input(prompt))
user_inputs.append(input(prompt))
user_inputs.append(input(prompt))
```
</details>
</details>

---

## Program output
The principal way to send information in the other direction (from the program
to the user) is the `print()` function. As with `input()`, we can look at its
`help` text, which is:
```
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)

    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default to a space.
    end:   string appended after the last value, default to newline.
    flush: whether to forcibly flush the stream.
```
Again, we can see that `print()` is a built-in function. This function can take
any number of arguments, with a few being named with default values (`sep=' '`,
`end='\n'`, `file=sys.stdout`, `flush=False`) that we'll leave for later. For
now, we can consider `print()` to -- as the name indicates -- print out its
arguments. Each argument is printed out separated by a space (`sep=' '`), and
after everything is printed, a "newline" character (`end='\n'`) is printed,
which moves the terminal cursor to the beginning of the next line (think of how
a typewriter works). 

Take the following example:
```Python
my_var1 = 5
my_var2 = "hello"
my_var3 = True
print(my_var1, my_var2, my_var3)
```
When executed, this will produce the output
```
5 hello True

```
and we can note that if we wanted, we could add as many values to print out (of
any type!) as we wanted. And we could also tweak the values of `sep` and `end`,
too: If we pass the arguments `sep=';'` and `end='!'` instead, as in
```Python
print(my_var1, my_var2, my_var3, sep=";", end="!")
```
then we'd see
```
5;hello;True!
```

As you do more scripting in Python, you'll see that `print()` is a *very*
important tool for learning exactly what your program is doing at each point in
its execution.

---
<details>
<summary></strong>Check your understanding</strong></summary>

Now that we can have our programs talk back to users, let's expand on the result
of the previous "Check your understanding" by having the program confirm that it
knows what the user typed. Edit your program so that after accepting the inputs,
it prints them all back out with a "you typed: " tacked on to the beginning of
each one. Remember, the output of the `input()` function is just a regular
`str`, and can be manipulated as such!

<details>
<summary>Answer</summary>

The addition to your program should look something like this:
```Python
# here's where the program you wrote for the last part goes

# each item of `user_inputs` is just a str, so it can be concatenated with
# another str with the `+` operator
# set the desired prefix here
prefix = "you typed: "

# print out each input with the prefix
print(prefix + user_inputs[0])
print(prefix + user_inputs[1])
print(prefix + user_inputs[2])
```
</details>
</details>

---

