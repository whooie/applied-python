# Data storage and containers

Now we'll cover Python's container types. As the name implies, these are types
that contain other types. Python has four such (built-in) types, `list`,
`tuple`, `dict`, and `set`. This will also be a good opportunity to draw the
important distinction between a *type* and *objects of that type*. As we go
through these, it will additionally be useful to keep track of three properties
that each
container type has:
- Ordered-ness: the quality of having a built-in order known to Python; in other
  words, whether contained objects can be accessed with an index.
- Mutability: the quality of being able to change the structure of the container
  or the objects it contains.
- Uniqueness of items: whether the container can hold multiple instances of the
  same value.

## Contents
<!-- toc -->

## `list`
We'll talk about `list`s first. In a couple of the examples from the last page,
we created a lot of variables with similar names. `my_num1`, `my_num2`,
`my_num3`, and `my_num4` all refer to numbers that are related to each other, so
it would make sense to collect them all together so that we don't have to keep
track of four independent names in the rest of the program, wouldn't you say?
`list`s are the primary way that Python provides to do this. Specifically,
`list`s are ordered, mutable collections of non-unique data, all grouped under
one variable name, and are denoted with `[ , ]` square brackets. Let's break
down what this means.

Python respects a `list`'s order. This means that whenever a `list` is declared,
for example with
```Python
my_list = [1, 5, 'a', 3, 1e6, 5, 2j, None, True]
```
Python will assign to each object contained in the `list` an index starting with
the leftmost at zero, similar to `str`s. And like `str`s, `list`s can be indexed
and sliced using the same `[...]` notation. But right away we can notice a
couple more things about `list`s. First, `list`s can clearly contain more than
one type of object. In the above declaration, we can see that `my_list` contains
every object type we've talked about so far! (And, although not explicitly
stated, `list`s can also contain every other object type we *will* talk about,
as well.) This is usually not considered best practice, however, and should be
avoided in preference of working with lists that contain only one object type.
Second, we can see that `list`s can contain multiple of the same object, as
shown by the two `5`'s in `my_list`.

Also, the operations `+` and `*` are defined for `list`s in a similar manner as
`str`s:
```Python
[1, 5] + [3, 4] == [1, 5, 3, 4]  # evaluates to True
3 * [1, 5] == [1, 5, 1, 5, 1, 5] # evaluates to True
```

And so now we can ask an important question: If `list`s and `str`s have such
similar behavior, then (aside from `list`s being able to hold multiple types),
how is a `str` any different from a `list` containing only single-character
`str`s?

The answer lies in the *mutability* of these two container types. Simply put,
`list`s can be altered ("mutated"), while `str`s cannot. In general, this is
done by assigning values to an indexing expression just like you would to a
normal variable:
```Python
my_list = [1, 2, 3, 4, 5]
my_list[3] = 8
my_list == [1, 2, 3, 8, 5] # evaluates to True
```
If you tried to do something similar to a `str`, Python would kick back an
error:
```Python
my_str = "hello"
my_str[3] = "k" # this generates a TypeError!
```

Another way to mutate a `list` is by `append`ing, `insert`ing, or `pop`ing
values, which is done through a special syntax we haven't seen before:
```Python
my_list = [1, 2, 3, 4, 5]
my_list.append(6) # add a 6 onto the end of my_list
my_list == [1, 2, 3, 4, 5, 6] # evaluates to True
my_list.insert(1, 10) # insert a 10 into my_list at index 1
my_list == [1, 10, 2, 3, 4, 5, 6] # evaluates to True
my_list.pop(3) # remove the value at index 3 from my_list
my_list == [1, 10, 2, 4, 5, 6] # evaluates to True
```
This syntax, using the `.` character, tells Python you want to access an
*attribute* of whatever precedes it with a name that is whatever comes after it.
In this case, the lines above tell Python we want to access attributes of
`my_list` that are called `append`, `insert`, and `pop`, which are functions, as
shown by the immediately following parentheses that enclose the function
arguments. We haven't gotten to functions properly yet (they're coming in the
next unit), but the short of it is that they are names for short snippets of
code that implement some behavior (in this case, `append`ing, `insert`ing, and
`pop`ping to/from `my_list`) that depends on whatever lies inside the
parentheses (the function arguments). In particular, an attribute of an object
that is a function is called a *method*.

Now is also a good time to expand further on the difference between the `==`
(equality) and `is` (identity) operators that was mentioned on the previous
page. In a sentence, the `==` operator tests if two objects are equal, but the
`is` operator tests if they are *literally the same*. The two are effectively
the same for the basic types discussed on the previous page (in fact they'll
always give equal values for comparisons between those types), but are different
in the vast majority of cases for containers. To illustrate this, suppose we
have three `list`s:
```Python
list_x = [1, 5]
list_y = [2, 3]
list_z = [1, 5]
```
and suppose we wished to try both operators on the possible pairings. First, we
can try `list_x == list_y` and `list_x is list_y`, which both evaluate `False`
as expected. But when we try `list_x == list_z` and `list_x is list_z`, we see
something interesting: the first evaluates to `True`, but the second evaluates
to `False`!
```Python
list_x == list_z # evaluates to True
list_x is list_z # evaluates to False

# can also be seen for comparison to a literal list:
list_x == [1, 5] # evaluates to True
list_x is [1, 5] # evaluates to False
```
Why is this? It's because while `list_x` and `list_z` may both
contain the same values (every object of basic type literally `is` another as
long as their values are equal; e.g. `5 is 5`), they are still different
instantiations of the `list` type, and so aren't exactly the same as each other.
On the other hand, we can find a case where an `is` comparing two `list`s
evaluates to `True` by assigning one of these two a new variable:
```Python
list_a = list_x
list_a is list_x # evaluates to True
```
For mutable container types, this is a very important comparison to make because
it is an indicator of whether mutating one variable will also mutate another, as
we can see here:
```Python
list_a.append(9)
list_a == [1, 5, 9] # evaluates to True
list_x == [1, 5, 9] # evaluates to True
```
In this example, both `list_a` and `list_x` mutate when the value is appended to
`list_a` because they are literally the same object.

As we've seen, `list`s are very instructive! With all this out of the way,
though, we can finally move on to the other built-in container types that Python
has to offer.

---
<details>
<summary><strong>Check your understanding</strong></summary>

What is the value of `my_list` at the end of these manipulations? Try to figure
it out using a pen and paper before plugging it all into Python!
```Python
my_list = [1, 5, 7, 2]
my_list = my_list + [8, 10]
my_list.insert(4, 3)
my_list = my_list[1::2]
my_list.append(15)
my_list[0] = 42
my_list = my_list[::-1] + my_list[::3]
```

<details>
<summary>Answer</summary>

Here's the value of `my_list` after each of the steps above:
```Python
my_list = [1, 5, 6, 2]                  # my_list == [1, 5, 7, 2]
my_list = my_list + [8, 10]             # my_list == [1, 5, 7, 2, 8, 10]
my_list.insert(4, 3)                    # my_list == [1, 5, 7, 2, 3, 8, 10]
my_list = my_list[1::2]                 # my_list == [5, 2, 8]
my_list.append(15)                      # my_list == [5, 2, 8, 15]
my_list[0] = 42                         # my_list == [42, 2, 8, 15]
my_list = my_list[::-1] + my_list[::3]  # my_list == [15, 8, 2, 42, 42, 15]
```
</details>
</details>

---

## `tuple`
`tuple` is Python's *ordered*, *non-unique*, *immutable* container type, and is
declared similarly to `list`s except with parentheses (`(` and `)`) instead of
brackets (`[` and `]`). Note that in the case of a one-item `tuple` (e.g.
`(5)`), there's ambiguity in meaning between the `tuple` and a simple expression
surrounded by parentheses, so to create a one-item tuple one comma is required,
as in `(5,)`. While this is a perfectly valid way to declare a `tuple`, most
programmers will prefer something closer to `tuple([5])` for the readability of
their code, however.

Like `list`s, `tuple`s represent simple sequences of objects, but *unlike*
`list`s, they cannot be mutated. This means `tuple`s lack the `append`,
`insert`, and `pop` methods mentioned above for `list`s, and can't have their
elements reassigned:
```Python
my_tuple = (5, 6, 7, 8, 9)
my_tuple[0] = 10 # raises an error!
```

So when should one use `list`s and when should one use `tuple`s? Here's a good
rule of thumb: if you're constructing a sequence of things and you think there's
a chance you might need to rearrange them, add to them, or subtract from them,
use a `list`. If the sequence is already built and won't need to be altered,
then use a `tuple`.

## `set`
`set` is Python's *unordered*, *unique*, *mutable* container type, and are meant
to conform to the usual rules surrounding the more general notion of a set from
mathematics. Just like in mathematics, `set`s are denoted using curly braces
(`{` and `}`), e.g.
```Python
my_set = {1, 2, 3, 4, 5}
```

As we've noted, `set`s are unordered, so they can't be indexed with the `[...]`
notation for `list`s and `tuple`s. Moreover, each element of a `set` is required
to be unique: At the time of declaration, only the unique elements in the `set`
are included in it, and so
```Python
{1, 2, 3, 4, 5, 5} == {1, 2, 3, 4, 5} # evaluates to True
```
As a side note, this can be used as an easy filter to find all the unique
elements of any `list` or `tuple`:
```Python
set([1, 1, 5, 3, 3, 4, 7, 7, 7]) == {1, 3, 4, 5, 7} # evaluates to True
set((1, 1, 5, 3, 3, 4, 7, 7, 7]) == {1, 3, 4, 5, 7} # evaluates to True
```

The `set` type also provides the usual union ($\cup$), intersection ($\cap$) and
difference ($/$) operations similar to the `append`, `insert`, and `pop`
operations on `list`s. These operations are different, however, in that they
compute and return the unions, intersections, and differences with other `set`s
and return the result as a new `set`, rather than mutating the existing one:
```Python
set_A = {1, 2, 3, 4, 5}
set_B = {4, 5, 6, 7, 8}

set_A.union(set_B) == {1, 2, 3, 4, 5, 6, 7, 8}
set_A.intersection(set_B) == {4, 5}
set_A.difference(set_B) == {1, 2, 3}
set_A.symmetric_difference(set_B) == {1, 2, 3, 6, 7, 8}
```
If you *do* want to mutate the existing sets, though (which we call an
"in-place" operation), the analogous ways to perform these operations in place
are via
```
union                   -> update
intersection            -> intersection_update
difference              -> difference_update
symmetric_difference    -> symmetric_difference_update
```

## `dict`
The `dict` type (short for "dictionary") take the idea of a collection of
objects one step further by thinking of each object as a mapping from an input
to an output. Just like you might use a real dictionary to translate a word in
English to one in Spanish, `dict`s some "key" and map it to some "value" through
the syntax `{key1: value1, key2: value2, ...}`. As this syntax suggests, `dict`s
can be thought of as a `set` of key-value pairs. Like `set`s, `dict`s are also
*unordered*, *mutable*, and *unique*, with the addendum that only each key has
to be unique, rather than each value or each key-value pair.

The keys and values can be almost anything, as long as they obey two rules:
- Each value in the `dict` must have at least one corresponding key.
- While values can be any type, the keys of a `dict` cannot be mutable (if a
  value were stored under a key and then that key mutated, the value would be
  effectively lost!) -- so, for example, `int`s, `str`s, and `tuple`s are all
  good keys for a `dict`, but `list`s and `set`s are not.

Take a look at a couple examples:
```Python
dict_en_sp = {
    "hello": "hola",
    "goodbye": "adiós",
    "my name": "me llamo",
    "where is the library": "donde está la biblioteca"
}

dict_num_word = {
    0: "zero",
    1: "one",
    2: "two",
    3: "three"
}
```

The first dictionary above defines a mapping from a small set of phrases (as
`str`s) in English to their corresponding phrases Spanish, while the second
maps a few `int`s to their corresponding word forms. To access these mappings,
we use the `[...]` syntax to feed in a key, and get its value back out:
```python
dict_en_sp["hello"] # evaluates to "hola"
dict_num_word[3] # evaluates to "three"
```

There are two important differences between `list`s and `dict`s with regard to
this syntax, however. The first is that `dict`s cannot be sliced. The second is
that `dict`s support value assignment to keys that do not already exist in the
`dict`. If we wanted to expand `dict_en_sp` or add more numbers to
`dict_num_word`, we could do this simply with the `=` operator:
```python
dict_en_sp["the spider"] = "la araña"
dict_en_sp["the python"] = "la pitón"
dict_num_word[4] = "four"
dict_num_word[100] = "one hundred"
```

---
<details>
<summary><strong>Check your understanding</strong></summary>

Try creating your own `dict` mapping each letter of the alphabet to a number,
starting with `"a": 1`, `"b": 2`, and going all the way to `"z": 26`. Note that
you can do this in two ways: 1) by declaring the entire `dict` all at once using
the `{...}` syntax, or 2) by starting with an empty `dict` (something like
`my_dict = dict()`) and progressively adding more key-value pairs with the
`my_dict[...] = ...` syntax.

<details>
<summary>Answer</summary>

We'll create this dictionary in both ways. Using the first method:
```Python
letter_to_num = {
    "a": 1,
    "b": 2,
    "c": 3,
    "d": 4,
    "e": 5,
    "f": 6,
    "g": 7,
    "h": 8,
    "i": 9,
    "j": 10,
    "k": 11,
    "l": 12,
    "m": 13,
    "n": 14,
    "o": 15,
    "p": 16,
    "q": 17,
    "r": 18,
    "s": 19,
    "t": 20,
    "u": 21,
    "v": 22,
    "w": 23,
    "x": 24,
    "y": 25,
    "z": 26,
}
```

Using the second:
```Python
letter_to_num = dict()
letter_to_num["a"] = 1
letter_to_num["b"] = 2
letter_to_num["c"] = 3
letter_to_num["d"] = 4
letter_to_num["e"] = 5
letter_to_num["f"] = 6
letter_to_num["g"] = 7
letter_to_num["h"] = 8
letter_to_num["i"] = 9
letter_to_num["j"] = 10
letter_to_num["k"] = 11
letter_to_num["l"] = 12
letter_to_num["m"] = 13
letter_to_num["n"] = 14
letter_to_num["o"] = 15
letter_to_num["p"] = 16
letter_to_num["q"] = 17
letter_to_num["r"] = 18
letter_to_num["s"] = 19
letter_to_num["t"] = 20
letter_to_num["u"] = 21
letter_to_num["v"] = 22
letter_to_num["w"] = 23
letter_to_num["x"] = 24
letter_to_num["y"] = 25
letter_to_num["z"] = 26
```
</details>
</details>

---

## Cheat sheet
Finally, here's a small table listing the container types covered on this page
with respect to their properties:

| Container type | Ordered? | Mutable? | Unique items? |
|:---------------|:---------|:---------|:--------------|
| `list`         | Yes      | Yes      | No            |
| `tuple`        | Yes      | No       | No            |
| `set`          | No       | Yes      | Yes           |
| `dict`         | No       | Yes      | Yes           |

