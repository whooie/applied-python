# Basic types and variables

Nearly all programming languages differentiate between types of data. Different
languages have different standards for how their types are dealt with and
interact with each other (Python is particularly flexible in this regard), but
they all require the programming to think about, for instance, the difference
between the number `12` and the letter `'a'`. This is partly to help the
computer manage all the bits and bytes that make up a program while it's
running, and partly because some operations simple don't make sense for some
types. (What would it mean to compute the sum `12 + 'a'`?)

This page will mainly detail Python's basic, built-in data types. These types
are, in another word, atomic, in that they are indivisible building blocks from
which larger data types are (usually) built and operated on. In particular,
basic-ness is a useful distinction to make because, as we'll see in the next
page, there are built-in (as opposed to user-defined) types that contain other
types. (These other types are called "containers" for obvious reasons.)

Further down, we'll also talk a bit about how to work with these types and
values in the so-called "programmatic" way.

## Contents
<!-- toc -->

## Numeric types
First we'll deal with Python's numeric types, of which there are four: `int`,
`float`, `complex`, and `bool`. For the most part, all of these types can
interact with each other through standard arithmetic operations (`+`, `-`, `*`,
`/`), with a few addenda that will be mentioned at the end of this section.

### `int`
`int`s are how Python represents integer numbers (that is, all the whole numbers
from minus infinity to positive infinity). `int`s can add to, subtract from, and
multiply each other, but there's a bit of fine print when it comes to division.
In general, the division of two integers will not itself be another integer
(consider the operation `5 / 2`), so Python actually has two kinds of division,
denoted by `//` and `/`. The first kind is what you should use when you want to
divide two `int`s and get another `int` out: it computes the full division and
discards any non-integer remainder (`5 // 2` equals `2`). The second kind is
what you should use if you want the full answer as floating-point number, which
brings us to...

### `float`
`float`s represent floating-point numbers, or numbers that may have a
non-integer part. `float`s can interact with all other numbers in the way you'd
normally expect, but can be written in two different but functionally equivalent
ways. The first is the standard way, e.g. `250.734`, and the second is with
scientific notation, e.g. `2.50734e2` as shorthand for 2.50734 \* 10^2 or, in
Python syntax, `2.50734 * 10**2`.

### `complex`
`complex`s are actually two `float`s bundled together to represent the real and
imaginary parts of a single complex number. To use a `complex`, you can write it
using `j` (Python denotes the imaginary unit $i$ with a `j`) notation, e.g. `1.5
+ 0.4j`, `-2.6 + 1e6j`. When two complex numbers are added, subtracted,
multiplied, or divided together, the imaginary components obey the usual rules,
i.e. `1j * 1j` is equal to `-1`.

### `bool`
`bool`s (short for Boolean values) represent truth values for logical
expressions, and can be either `True` or `False`. When interacting with the
other number types, `bool`s take on the values `False = 0` and `True = 1`. You
can also work with `bool`s through *boolean expressions*, which are like regular
arithmetic ones, except they evaluate to either `True` or `False` instead. For
example, say we want to perform some checks on the number `5` to test its value.
To this purpose, we can use Python's `==`, `!=`, `<`, `<=`, `>`, and `>=`
operators which are equivalent to asking whether a value is equal, not equal,
less than, less than or equal to, greater than, and greater than or equal to
another value. When used, each of these symbols will evaluated to either `True`
or `False`:
```python
5 == 8 # evaluates to False
5 != 8 # evaluates to True
5 <  8 # evaluates to True
5 <= 8 # evaluates to True
5 >  8 # evaluates to False
5 >= 8 # evaluates to False
5 == 5 # evaluates to True
5 != 5 # evaluates to False
5 >= 5 # evaluates to True
```

`bool`s can also interact with other `bool`s through standard logical operations
like `and`, `or`, and `not`. The action of these operations is usually expressed
through a "truth" table, where we look at all the possible values of two `bool`s
`P` and `Q`, and report the corresponding value of some Boolean operation (known
as a "truth table"):

| `P`     | `not P` |
|:--------|:--------|
| `True`  | `False` |
| `False` | `True`  |

<br/>

| `P`     | `Q`     | `P and Q` | `P or Q` |
|:--------|:--------|:----------|:---------|
| `True`  | `True`  | `True`    | `True`   |
| `False` | `True`  | `False`   | `True`   |
| `True`  | `False` | `False`   | `True`   |
| `False` | `False` | `False`   | `False`  |

### Arithmetic operators
In addition to `+`, `-`, `*`, and `/`, Python offers a few more operators to
denote arithmetic operations (you've already seen one of them in `//` for
integer division). Describing them in as much detail is probably not worth your
time, so here's a short list of them:

| Operator | Description                         |
|:---------|:------------------------------------|
| `+`      | Addition                            |
| `-`      | Subtraction                         |
| `*`      | Multiplication                      |
| `/`      | Division                            |
| `//`     | Integer division                    |
| `%`      | Remainder (positive-or-zero modulo) |
| `**`     | Exponentiation                      |
| `@`      | Matrix multiplication               |
| `and`    | Boolean AND                         |
| `or`     | Boolean OR                          |
| `not`    | Boolean NOT                         |
| `>`      | Greater than                        |
| `>=`     | Greater than or equal to            |
| `<`      | Less than                           |
| `<=`     | Less than or equal to               |
| `==`     | Equality                            |
| `is`     | Identity                            |

Note that these come with a few original notes:
- All the more usual operators (those towards the top of this table) are
  evaluated following the order of operations you'd normally expect, but if
  you're ever unsure of the order, don't be afraid to use parentheses!
- The `//` and `%` operators can't be used with `complex`. These come from the
  facts that:
    - there's no (standard) way to extend integers to the complex plane, and
    - there's no (standard) way to order complex numbers such that the `floor`
      and `ceil` functions can be defined for all complex numbers. (Likewise,
      the boolean comparison operators, except for `==` and `is`, can't be used
      on `complex`, either.)
- `@` is usually not used during normal operation, except when you're working
  extensively with array or matrix libraries, or if you want to define it for a
  custom class, as we'll cover in the [numpy/scipy][numpy/scipy] and  
  [object-oriented programming][oop] pages.
- There's a somewhat-subtle difference between the `==` and `is` operators that
  we'll cover on the next page.
- There are actually more operators, but these cover 99% of the operations most
  users will require. You can find more information on the other operators by
  typing `help()` in the Python console, and then any of the above in the
  interactive dialogue.

[numpy/scipy]: ../04_common-libraries/numpy-scipy.md
[oop]: ../02_paradigms-and-data-structures/object-oriented.md

---
<details>
<summary><strong>Check your understanding</strong></summary>

- What are the main differences between `int`s, `float`s, and `complex`s? What
  are the output types of the following expressions?
    - `5 * 3`
    - `2 * 7.0`
    - `8 * (3 + 10j)`
    - `False * 3.14`
    - `7 // 3`

  <details>
  <summary>Answer</summary>
  
  - `int`
  - `float`
  - `complex`
  - `float`
  - `int`
  </details>
- Fill out a truth table for the expression `(not P) or (Q and R)` where `P`,
  `Q`, and `R` are all `bool`s.

  <details>
  <summary>Answer</summary>
  
  | `P`     | `Q`     | `R`     | `(not P) or (Q and R)` |
  |:--------|:--------|:--------|:-----------------------|
  | `True`  | `True`  | `True`  | `True`                 |
  | `True`  | `True`  | `False` | `False`                |
  | `True`  | `False` | `True`  | `False`                |
  | `True`  | `False` | `False` | `False`                |
  | `False` | `True`  | `True`  | `True`                 |
  | `False` | `True`  | `False` | `True`                 |
  | `False` | `False` | `True`  | `True`                 |
  | `False` | `False` | `False` | `True`                 |
  </details>
</details>

---

## `None`
`None` is the final "true" base type in Python. (For why I make this
qualification, see the [`str` section][str].) Much by intention, it's a very
simple type: Pretty much any time you try interacting with it in a complex
manner, Python will kick back an error (I invite you to try this in the Python
console!), and the reason for this is because it's Python's way of representing
the idea of nothing. (This is distinct from the concept of zero, which still
corresponds to a definite quantity.) `None` will still be useful to keep in mind
for scripting and best-practice reasons later on, but for now just think of it
as a footnote trailing behind all of Python's other, interesting types.

[str]: #str

## Variables
What is a variable? Simply put, it's a way to store a value in the computer's
memory so that it can be referred back to at a later time. At its most basic, a
variable is just a name you give to some value, just like in regular math. In
Python, values are assigned to names with the `=` operator like so:
```Python
my_num1 = 5
my_num2 = 8
```

Now we have the values `5` and `8` stored in the variable names `my_num1` and
`my_num2`, respectively. Notice that, unlike the usual your usual math homework,
our variable names can be more than one letter, and include special characters
like `_` and even numbers. In fact, anything not beginning with a number or
containing any punctuation other than `_` is a valid variable name in Python, so
you could even set `我 = 5` and `你 = 8` if you wanted (for the sake of code
readability, though, please don't)!

Now that we've assigned values to these names, we can use them in any situation
where we might otherwise use the values they refer to. For example, we can
perform all the arithmetic operations on them, and even assign them to other
variables:
```Python
my_num1 + my_num2
my_num1 * my_num3
my_num1 < my_num2
...
my_num3 = my_num1
my_num4 = my_num3 + my_num2
my_bool = my_num1 == my_num2
```
Notice that `=` (single equals) is *variable assignment*, while `==` (double
equals) is the *equality operator*. Being careful about which of these is used
and where will save you a few headaches down the line.

## `str`
As a lead-in to the next page, I mention `str` as a sort of halfway point to the
non-basic (container) built-in types to follow because even though you'll likely
see them mentioned as a basic type in Python by other sources, `str`s are
fundamentally quite similar to container types in behavior. In fact, most other
languages consider them to be more of a container type than a basic one. And as
we'll see in [the next unit][functions], the behavior of a type is really what's
important.

`str`s are Python's string type, which represents ordered sequences of
characters that do not hold any numerical value. While the most obvious example
of a string is a collection of alphabetic characters (plus punctuation) like
`"hello world!"`, `str`s may also contain numbers (as in `"14820"`) or other
unicode characters (as in `"你好世界！"`).

Python respects a `str`'s order. This means that whenever a `str` is declared,
for example with
```Python
my_str = "hello world!"
```
Python will assign each character in the string an index starting with the
leftmost at zero. So under this convention, we'd say that `my_str` contains a
`"h"` at its zeroth index, an `"e"` at its first index, and so on, until we get
to the `"!"` at its eleventh index. These indices are how Python expects you to
refer back to the individual objects in the string, using the `my_str[...]`
indexing notation. For example, to refer to the third index of `my_str`, you'd
write `my_str[3]`. This also works with negative integers, which index from the
rightmost character at `-1` (so `my_str[-3]` would refer to `"l"`).

One important thing to note here is that each of the characters of any `str` are
themselves also `str`s, even though they're treated syntactically as though
they're only a single part of the rest of the string -- thus one can see why
they're a sort of halfway point between a true basic type and a full container.

You can also refer to the characters over a range of indices using the `[:]`
notation. To refer to indices `1` through `3` of `my_str`, you'd write
`my_str[1:4]`, where the `:` character indicates to Python that you want a range
of indices, rather than just a single one. Notice that we have `4` at the upper
end of the range, rather than `3`. This is because in Python, the convention for
naming ranges of values is to have the upper end be non-inclusive; that is, in
this case, we name all the indices starting at `1`, up to *but not including*
`4`. Optionally, you can also specify the spacing (or "step size") of the
indices referred to by the slicing notation using another `:` character. By
default, Python assumes you want every single integer in the range you specify,
but if you wanted, you could take every other or every third character in
`my_str` with `my_str[0:12:2]` or `my_str[0:12:3]`, respectively. You could also
reverse the order of the string with `my_str[-1:-13:-1]`. In fact, you don't
even have to specify all those numbers if you don't want to: The third number
and the second `:` can be omitted to assume a step size of 1 as we've already
seen, or the first two numbers can be omitted to assume starting and ending
points corresponding to the appropriate end of the string.

Here's a short snippet summarizing all of the indexing and slicing rules
mentioned above:
```Python
my_str = "hello world!"

# the following evaluate to True:
my_str[4] == "o"
my_str[-4] == "r"
my_str[4:10] == "o worl"
my_str[:5] == "hello"
my_str[5:] == " world!"
my_str[:9:3] == "hlw"
my_str[::-1] == "!dlrow olleh"
```

One more thing we can say about `str`s is that they have a few arithmetic
operations defined. Specifically, `+` and `*` are defined for `str`s and `int`s,
respectively: `+` concatenates two `str`s together, and `*` repeats a given
`str` some number of times, as below.
```Python
"hello " + "Bob!" == "hello Bob!" # evaluates to True
8 * "na " + "Batman!" == "na na na na na na na na Batman!" # evaluates to True
```

[functions]: ../01_control-flow/functions.md

---
<details>
<summary><strong>Check your understanding</strong></summary>

How would you go about inserting one `str` at some index in the middle of
another `str`? For example, say you want to make some insertion into `my_str`
(defined above) so that it reads `"hello and goodbye world!"` without just
reassigning the variable (this requires you to use slicing operations in
combination with `+`!).

<details>
<summary>Answer</summary>

One way to do this is by splitting `my_str` into two halves at the point of
insertion, and then concatenating the two with the bit you want to insert in the
middle:
```Python
# remember to include the additional space in " and goodbye"!
my_str2 = my_str[:5] + " and goodbye" + my_str[5:]
my_str2 == "hello and goodbye world!"
```
</details>
</details>

---

