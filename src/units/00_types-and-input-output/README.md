# Unit 0: Types and input/output

Python is an interpreted, high-level programming language known for its
flexibility and wide applicability in addition to its accessibility to new
programmers. This unit introduces the basic objects that Python can operate on,
as well as some simple ways to feed information to and get information from your
Python programs.

Contents:
- [Basic types and variables](types.md)
- [Data storage and containers](containers.md)
- [Program input/output](program-io.md)
- [Exercises](exercises.md)

