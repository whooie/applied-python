# Exercises

## Contents
<!-- toc -->

## 1. Hello, user!
Using what we've learned about the `input()`, and `print()` functions, as well
as how to concatenate `str`s, we can start off with a simple program that asks
for the user's name, and prints a greeting. Write a program that asks the user
to type in their name with the prompt `What is your name?`, and then prints out
`Hello, <name>!`, where `<name>` is whatever the user typed in.

## 2. String insertion and deletion
As we've already said, `str`s are immutable objects -- this precludes the
existence of the `append`, `insert`, and `pop` methods that we have access to
for `list`s. But while these methods may not exist, we can still implement these
operations as long as they produce new `str`s rather than modifying existing
ones in place. Write a program that defines an initial string `"hello world!"`
and performs the equivalents of `append("!")`, `insert(4, "@")`, and `pop(2)` on
it, printing out the results for each operation. Your program should look
something like this:
```Python
my_str = "hello world!"

### append("!")
# <your code here>

print(my_str_appended) # should print out: hello world!!

### insert(4, "@")
# <your code here>

print(my_str_inserted) # should print out: hell@o world!

### pop(2)
# <your code here>

print(my_str_popped) # shold print out: helo world!
```

## 3. `list` element swap
Write a program in which an arbitrary `list` is created and stored in a
variable, some operations (which you should determine for yourself) are
performed, and by the end, the `list` is `print`ed out with two of its elements
swapped. Your program should look something like this:
```Python
# the initial state of the list
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

# the two indices to swap
swap_idx_1 = 3
swap_idx_2 = 8


# <your code here>


print(my_list) # should print out `[1, 2, 3, 9, 5, 6, 7, 8, 4, 0]`
```
Make sure that your code modifies `my_list` *in place*, that it will work with
different initial lists and swap indices, and that you can change `my_list`,
`swap_idx_1`, and `swap_idx_2` *only* and still have your program print out the
correct answer. You may assume that `swap_idx_1` and `swap_idx_2` will always be
valid indices.
<details>
<summary>Hint</summary>

Ultimately, the goal of the swap is to reassign the value of one `list`
index to the other, but if you try simply doing
```Python
my_list[swap_idx_1] = my_list[swap_idx_2]
my_list[swap_idx_2] = my_list[swap_idx_1]
```
you get an incorrect result. Why is this? If you're running into trouble, you
should step through your program and *be the computer*: step through your code
line by line and think about what happens to the values of all the variables
(including `list` indices).
</details>

## 4. Find the distance between two points
Points in the Cartesian plane can be written as two-item `tuple`s, like so:
```Python
point_1 = (3, 4)
point_2 = (21, 3)
point_3 = (3.2, 9.5)
```
where conventionally, the first number in each `tuple` corresponds to the
point's $x$-coordinate, and the second to its $y$-coordinate. We can take the
*distance* between two points $P$ and $Q$ to be their *Euclidean distance*,
$$
d_E\big( P, Q \big)
    = \sqrt{ \big( P_x - Q_x \big)^2 + \big( P_y - Q_y \big)^2 }
$$
where $P_x$ and $P_y$ denote the $x$- and $y$-coordinates of $P$, and similarly
for $Q$.

Write a program that defines three arbitrary points and finds the Euclidean
distances between each pair of points, and verify that these distances
(corresponding to the side lengths of the triangle formed by the points),
satisfy the triangle inequality, namely that the sum of any two of these
distances is greater than or equal to the third. Your program should look
something like this:
```Python
# we need this line in order to compute square roots!
# `sqrt(x)` computes the square root of any numerical value `x`.
from math import sqrt

# you can choose your points to be whatever you want
point_1 = (..., ...)
point_2 = (..., ...)
point_3 = (..., ...)

### distance(point_1, point_2)
# <your code here>

print(d_12) # should print out the correct distance between points 1 and 2

### distance(point_2, point_3)
# <your code here>

print(d_23) # should print out the correct distance between points 2 and 3

### distance(point_3, point_1)
# <your code here>

print(d_31) # should print out the correct distance between points 3 and 1

### trangle inequality
# your code should print out some indication that d_xy, d_yz, and d_zx satisfy
# the triangle inequality, where xyz is some cyclic permutation of 123 (e.g.
# 123, 312, 231).
```

