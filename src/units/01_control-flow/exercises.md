# Exercises

## Contents
<!-- toc -->

## 1. Fizzbuzz
This is a question sometimes included in job interviews to determine basic
competency. The other exercises I have here do much more for this, but I've
included it here because I thought it'd be kind of fun(ny).

Here's the exercise. Take all the positive integers up to 100 (i.e. 1, 2, 3,
..., 100). For each number, do the following: If the number is divisible by 3,
print out `Fizz`; if the number is divisible by 5, print out `Buzz`, and if the
number is divisible by both, print out `Fizz Buzz`. If the number is divisible
by neither 3 nor 5, print out the number instead. You'll probably find the `%`
modulo operator useful.

## 2. Simple recursion examples
Here we'll get some practice with recursion.

### 2.1 Fibonacci numbers
Aside from the example we saw calculating factorials in the main text, another
quintessential case where one can use recursion is in the calculation of
Fibonacci numbers. The Fibonacci numbers $F_n$ for $n = 1,\, 2,\, 3,\, \dots$
are defined like this:
$$
F_n
    = \begin{dcases}
        0
            & \t{if}~ n \leq 1
        \\
        1
            & \t{if}~ n = 2
        \\
        F_{n - 2} + F_{n - 1}
            & \t{otherwise}
    \end{dcases}
$$
In plain terms, each Fibonacci number is the sum of the two that come before it
in the sequence, except for the first and second, which are defined to be 0 and
1, respectively. Write a recursive function that will calculate the $n$-th
Fibonacci number.

### 2.2 Digital roots
The [digital root][digital-root] of any non-negative integer $n$ is found via
the following procedure:
1. Compute the sum $R$ of the individual digits of $n$
2. If $R$ is not more than one digit long, then this is the digital root of
   $n$. Otherwise, return to step 1, using $R$ as the number whose digits are
   summed.
For example, the digital root of 31 is $3 + 1 = 4$, while the digital root of
12345 is $1 + 2 + 3 + 4 + 5 = 15 \rightarrow 1 + 5 = 6$.

Write a function that computes the digital root of a given number recursively.

[digital-root]: https://en.wikipedia.org/wiki/Digital_root

### 2.3 Prime factorization
A number's *prime factorization* is the list of all prime numbers (which are
numbers that are only divisible by 1 and themselves) that can be multiplied
together to yield the original number. Consider the following examples:

| Number | Prime factorization |
|:-------|:--------------------|
| 18     | [2, 3, 3]           |
| 630    | [2, 3, 3, 5, 7]     |
| 284    | [2, 2, 71]          |
| 17     | [17]                |

Write a (recursive) function that computes all the prime factors of a given
number and returns them in a `list`.

This one may be a bit trickier to think about. The following are a few helpful
hints if you find yourself getting stuck:

---
<details>
<summary>Hint 1</summary>

It's usually helpful to start by identifying your base case(s). Under what
conditions (what kind of number passed to your function) can your function give
the correct answer by doing nothing (i.e. simply returning the number it was
passed)?
</details>

---
<details>
<summary>Hint 2</summary>

The base case we can identify for this function is when the number (call it $N$)
is prime -- the prime factorization of any prime number is simply a list
containing itself!

The next step is finding a way for the computer to *detect* the base case: what
test can we have it do to determine whether $N$ is prime?
</details>

---
<details>
<summary>Hint 3</summary>

For most numbers, the average size of their prime factors is pretty small --
usually it's a bunch of 2's, 3's, 5's, and the occasional 7. This suggests that
a naive (but effective!) way to test for the prime-ness of a number is by simply
looping through all the numbers $k$ smaller than $N$ (but greater than 1) and
testing whether each one evenly divides $N$. If you find some $k$ that divides
$N$, then you know that $N$ is not prime, and in fact have just found one of its
divisors. How can you use this fact to reduce the size of your input so that you
eventually work down to the base case?

Also, you should think about the order in which you loop through all the numbers
less than $N$. The most natural order in Python is starting from 2 and
increasing from there. In fact, this coincidentally guarantees that the first
divisor you find for $N$ will be prime. Think about why this is! What might
happen if you started at $N - 1$ and worked your way down to 2?

One other thing to think about is *how many* numbers in the range $2,\, \dots,\,
N$ really need to be checked. If it's less than $N - 1$, then you could save
some computation time by cutting your search off early!
</details>

---

## 3. Numbers in binary
Numbers, as we commonly see them in everyday communication, are written in what
is called a base-10, or *decimal* system. In this system, each digit of a number
is one of ten possible values (0 through 9), and corresponds a certain power of
10, ordered from greatest on the left, to least on the right:
$$
\begin{aligned}
    6180
          &= 6 \times 1000
        & &+ 1 \times 100
        & &+ 8 \times 10
    \\
          &= 6 \times 10^3
        & &+ 1 \times 10^2
        & &+ 8 \times 10^1
        & &+ 0 \times 10^0
    \\\pheq\\
    47
          &=
        & &
        & &+ 4 \times 10
        & &+ 7 \times 1
    \\
          &= 0 \times 10^3
        & &+ 0 \times 10^2
        & &+ 4 \times 10^1
        & &+ 7 \times 10^0
\end{aligned}
$$
(Hopefully you see the pattern.)

*Binary*, on the other hand, is a base-2 number system, where now each *binary
digit* ("bit" for short) is instead one of *two* possible values (0 or 1), and
corresponds to a certain power of *2*, ordered from greatest on the left to
least on the right:
$$
\begin{aligned}
    11010
          &=
        & &+ 1 \times 16
        & &+ 1 \times 8
        & &
        & &+ 1 \times 2
    \\
          &= 0 \times 2^5
        & &+ 1 \times 2^4
        & &+ 1 \times 2^3
        & &+ 0 \times 2^2
        & &+ 1 \times 2^1
        & &+ 0 \times 2^0
    \\\pheq\\
    100101
          &= 1 \times 32
        & &
        & &
        & &+ 1 \times 4
        & &
        & &+ 1 \times 1
    \\
          &= 1 \times 2^5
        & &+ 0 \times 2^4
        & &+ 0 \times 2^3
        & &+ 1 \times 2^2
        & &+ 0 \times 2^1
        & &+ 1 \times 2^0
\end{aligned}
$$

While these two systems may look different, it's important to keep in mind that
they are only different *representations* of numerical values, in much the same
way that two paintings may be created in different styles and still depict the
same object, or two words from different languages may refer to the same idea.
In this exercise, we'll be using binary notation (and conversions to and from
decimal notation) as an opportunity to practice using loops and some simple
logic.

### 3.1 Conversion to and from binary
First, we want to implement a way to convert between decimal and binary
representations of the same number. The binary representation that we'll use is
a `list` of bits, each of which is an `int` that is either `1` or `0`. The
convention we'll use for the ordering of these bits in each `list` is that the
order of the bits corresponds to the order in which they'd be written out on
paper (which is to say that `[1, 0, 0, 0] == 8`). This is called "small-endian"
order, where the bit corresponding to the **small**est power of 2 comes at the
**end**. We can take normal `int` objects as the decimal representation.

Your task is to write two functions `to_binary(...)` and `from_binary(...)`,
which implement the conversion from normal Python `int` objects to `list`s of
`int`s as well as the inverse conversion, respectively. The full signatures of
these functions should be
```Python
def to_binary(n: int) -> list[int]:
    ...

def from_binary(b: list[int]) -> int:
    ...
```
Here are a few examples of usage:
```Python
to_binary(6) == [1, 1, 0]
to_binary(17) == [1, 0, 0, 0, 1]
to_binary(39) == [1, 0, 0, 1, 1, 1]

from_binary([1, 0, 1]) == 5
from_binary([1, 1, 0, 1, 0]) == 26
from_binary([1, 0, 1, 1, 1, 0, 1, 0]) == 186
```

Since this is not, strictly speaking, a math class, here are a few hints for
how to approach both conversions:

---
<details>
<summary>Hint 1</summary>

It is a good idea to think about these conversions as a series of operations
that are ordered by the power of 2 corresponding to each bit of the number.
Since we know the structure of the `list`s we want to generate or convert (we
know they are in small-endian order, with the last element of the `list` always
corresponding to the 2^0's place), this can be mapped to a series of operations
on the *indices* of the `list`.
</details>

---
<details>
<summary>Hint 2</summary>

Considering this, we can express the conversion from binary back to decimal with
the formula
$$
    n
        = \sum\limits_{k = 0}^{N - 1} 2^k \times b_{N - 1 - k}
        = 2^0 \times b_{N - 1}
        + 2^1 \times b_{N - 2}
        + \cdots
        + 2^{N - 1} \times b_0
$$
where $b_k$ refers to the element at the $k$-th index of the input binary `list`
(i.e. `b[k]`), and $N$ is the number of elements in `b` (i.e. `len(b)`).
</details>

---
<details>
<summary>Hint 3</summary>

For the conversion from decimal to binary, it is useful to first determine how
many bits are required to represent the given number, `n`. This can be found
using a couple functions from the `math` module: `number_of_bits =
math.ceil(math.log2(n + 1))` (remember to `import` the `math` module for this!).
This number is important because it determines the bounds of the loop you'll
want to use to construct the correct `list`. The general algorithm looks like
this:
1. Let $L$ be the number of bits required to represent the input decimal
   number $n$ (`L = math.ceil(math.log2(n + 1))`).
1. Let $m$ be a dummy variable, initially set to $m = n$.
1. Initialize $b$ to be an empty `list`, with elements denoted by $b_i$.
1. For each integer $k$ in the range $[0, L)$:
    1. If $m \geq 2^{L - 1 - k}$, then:
        1. Append a $1$ to $b$.
        1. Set $m = m - 2^{L - 1 - k}$.
    1. Otherwise:
        1. Append a $0$ to $b$.
1. Done.
</details>

---

### 3.2 Counting in binary
Next, we'll implement a binary counter, which, given some number in a binary
representation, increments the number by 1. The addition of two binary numbers
looks almost exactly the same as it does for decimal numbers, except instead of
carrying "tens" over, we carry over "twos". For example,
$$
\begin{gathered}
    1 + 1 = 10
    \\
    101 + 10 = 111
    \\
    11 + 1 = 100
\end{gathered}
$$
Here, we can still add the numbers bit-by-bit like we would in decimal notation,
with the modification that the sum of two 1's in the $k$-th place "carry over"
as a 1 in the $k + 1$-th place and a zero in the $k$-th place:
$$
1 \times 2^k + 1 \times 2^k
    = (1 + 1) \times 2^k
    = 2 \times 2^k
    = 1 \times 2^{k + 1} + 0 \times 2^k
$$

Your task is to write a function that takes a `list` of 1's and 0's that gives
the binary representation of a number, and increases the value of that number by
1 *in place*; i.e.
```Python
def increment_binary(num: list[int]):
    ...
```
such that the usage of this function should follow
```Python
my_number = [1, 1, 0, 1, 0] # equal to 26
print(my_number) # prints out [1, 1, 0, 1, 0]

increment_binary(my_number)
print(my_number) # now prints out [1, 1, 0, 1, 1]

increment_binary(my_number)
print(my_number) # now prints out [1, 1, 1, 0, 0]
```
Note that these `list`s have their bits ordered so that, when printed out, the
1's and 0's are in the same order as they should appear above in the
introduction to this exercise.

Note that, in order to keep incrementing values to infinity, you'll sometimes
need to insert additional 1's at the beginning of the `list`. As a lead-in to
the next part of this exercise, ask (and answer) yourself:
- When is it the case that you need to insert a new 1 when
  `increment_binary(...)` is called?
- How many times can you call `increment_binary(...)` between these insertions?
  Or: given a `list` of five bits of all 0 (`[0, 0, 0, 0, 0]`), how many times
  can `increment_binary(...)` be called before you have to insert a 1? What
  about six bits? Seven? Is there a mathematical formula for the number of times
  `increment_binary(...)` can be called for any number $K$ of bits?

  ---
  <details>
  <summary>Hint:</summary>

  It's related to the `math.log2` operation that you used in the previous
  part, and counts the number of unique numbers that can be constructed from
  $K$ bits.
  </details>

  ---

### 3.3 Encoding strings with binary
Now that we're effectively able to map decimal representations of numbers to
their binary representations, we can take things one step further, and see how
computers (which ultimately think and operate *only* in binary) deal with
non-numerical pieces of data, like `str`s.

It turns out that doing this is conceptually actually pretty simple: We just
have to define a mapping from `str` characters to (decimal) numbers. Once we
have this, we can take a given `str`, apply it to each character in the `str`,
then apply the mapping we already have from decimal representations to binary
representations. We then stick all of the binary representations together into
one big `list`, and just like that, we have a way to represent `str` data in a
way that's understandable to computers!

The `str`-`int` mapping we will use can most naturally be represented using a
`dict`. This (simple) mapping will only cover the English alphabet and a couple
other characters used for punctuation, but can easily be extended to your
heart's desire. The mapping is defined as:
$$
\begin{aligned}
    \text{`~~'}
        &\mapsto 0
    \\
    \text{`a'}
        &\mapsto 1
    \\
    \text{`b'}
        &\mapsto 2
    \\
    &~~\vdots
    \\
    \text{`z'}
        &\mapsto 26
    \\
    \text{`,'}
        &\mapsto 27
    \\
    \text{`.'}
        &\mapsto 28
    \\
    \text{`!'}
        &\mapsto 29
    \\
    \text{`?'}
        &\mapsto 30
\end{aligned}
$$
In Python, you can define this for yourself using
```Python
chr_to_int = {
    **{chr(k + 96): k for k in range(1, 27)},
    " ": 0, ",": 27, ".": 28, "!": 29, "?": 30
}
```
(This is a compact form using a bit of material from a future unit, but it's
included here so that you don't have to type it all out yourself.)

With this defined, your task is now to write two more functions,
`str_to_binary(...)` and `binary_to_str(...)` that convert `str`s to and from
binary `list`s with the signatures:
```Python
def str_to_binary(s: str) -> list[int]:
    ...

def binary_to_str(b: list[int]) -> str:
    ...
```
One extra consideration we need here is that when combining binary
representations together into one long sequence of bits (called a *bitstring*),
we need each constituent to have a set length -- for example, `'a'` should be
represented as some total number $K$ of bits, even if many of them are leading
0's, which don't contribute to the actual numerical value. The reason for this
is because when interpreting the bitstring for conversion back to a `str`, there
will be ambiguity in how many bits should correspond to one character. What is
$K$ for this mapping? After you determine $K$, you should make sure you add
enough leading zeros to each `str` character's binary representation to make
each character be represented by $K$ bits in your implementation of
`str_to_binary(...)`.

## 4. Matrix multiplication
A *matrix* is a rectangular array of numbers arranged in rows and columns, all
of equal length. One important property of a matrix is its size: we say that a
$M \times N$ matrix has $M$ rows and $N$ columns. The *elements* of a matrix --
that is, the numbers it contains -- are denoted with two indices. A matrix of
size $M \times N$ (call it $A$) is commonly written as
$$
A
    = \begin{bmatrix}
        A_{0, 0}        & A_{0, 1}      & \cdots        & A_{0, N - 1}
        \\
        A_{1, 0}        & A_{1, 1}      & \cdots        & A_{1, N - 1}
        \\
        \vdots          & \vdots        & \ddots        & \vdots
        \\
        A_{M - 1, 0}    & A_{M - 1, 1}  & \cdots        & A_{M - 1, N - 1}
    \end{bmatrix}
$$

A few mathematical operations between matrices are commonly defined as well:
- **Addition of two matrices.** For two matrices $A$ and $B$ of equal size, the
  sum $C$ of adding the two ($C = A + B$) is found by adding each of their
  individual elements:
  $$
    C_{i, j}
        = A_{i, j} + B_{i, j}
  $$
- **Multiplication of a matrix by a scalar.** For a matrix $A$ and a regular
  number $c$, the product $M$ of the two ($M = c A$) is found by multiplying all
  of the elements of $A$ by $c$:
  $$
    M_{i, j}
        = c A_{i, j}
  $$
- **Multiplication of two matrices.** For two matrices $A$ and $B$ of
  "compatible" shapes $M \times K$ and $K \times N$ (where $M$, $N$, and $K$ are
  positive integers), respectively, the elements of the product $C$ of the two
  ($C = A B$) are found according to the formula:
  $$
    C_{i, j}
        = \sum\limits_{k = 0}^{K} A_{i, k} B_{k, j}
        = A_{i, 0} B_{0, j}
        + A_{i, 1} B_{1, j}
        + \cdots
        + A_{i, K - 1} B_{K - 1, j}
  $$

In Python, we can represent a matrix as a `list` of `list`s:
```Python
A = [
    [ A[0][0],     A[0][1],     ...,   A[0][N - 1]     ],
    [ A[1][0],     A[1][1],     ...,   A[1][N - 1]     ],
    [ ...,         ...,         ...,   ...             ],
    [ A[M - 1][0], A[M - 1][1], ...,   A[M - 1][N - 1] ],
]
```
where the elements `A[i][j]` can be arbitrary numbers. Your task is to write
three Python functions implementing each of these operations between matrices
and numbers written in this style:
```Python
Num = (int, float)

def matrix_add(A: list[list[Num]], B: list[list[Num]]) -> list[list[Num]]:
    # your implementation here
    ...

def matrix_scalar_mul(c: Num, A: list[list[Num]]) -> list[list[Num]]:
    # your implementation here
    ...

def matrix_mul(A: list[list[Num]], B: list[list[Num]]) -> list[list[Num]]:
    # your implementation here
    ...
```
When writing these functions, you may assume that each `list[list[Num]]` is
properly rectangular (that is, all of the inner `list`s contained in the outer
one are of equal length), but *not* that they are of compatible sizes -- you
must check for this yourself and raise an `Exception` if they aren't!

