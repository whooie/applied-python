# Unit 1: Control flow

So far, we've defined some types and played around with some basic operations in
simple programs. But all of this has really amounted to using Python as little
more than a fancy calculator. What we want from all of this is to use Python as
a full *programming language*. It's entirely possible to write programs where we
tell the computer to perform only a set number of operations before it
terminates, with each operation specified one by one as we've been doing. But in
an ideal world, we'd like the computer to be able to do more. Namely, we'd like
it to be able to make decisions and repeat tasks on its own without explicit
direction. This unit details the basic tools available to the programmer that
enable one to direct the computer to different sections of programs depending on
the nature of the program's inputs.

Contents:
- [Boolean values and logic](logic.md)
- [Automated iteration](loops.md)
- [Functions](functions.md)
- [Exceptions and error-handling](exceptions.md)
- [Exercises](exercises.md)

