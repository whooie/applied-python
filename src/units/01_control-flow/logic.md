# Boolean values and logic

The conditional statement lies at the heart of how computers make decisions.
When we talk about *logic* in programming, it's usually in a much more
operationally focused sense, much like the contrast between a function in
programming and a function in pure mathematics that we discussed in the previous
unit. In programming, we want to think about how computers, specifically, will
actually do logic and perform logical operations.

## Contents
<!-- toc -->

## Expressions
One important concept that we haven't talked about explicitly yet is the idea of
an *expression*, which in this context can best be described as some combination
of values (either literal, like `5` or `"a"`, or by way of a variable name
referring to a value), functions, and/or operators forming a piece of code that
can be evaluated to obtain another value.

In fact, we have already dealt with expressions: Any time you've set the value
of a variable, that's an expression on the right-hand side of the `=`. Any time
you write out a piece of code that denotes some kind of value -- including
simply typing out a `5` or a variable name -- that's an expression. Even things
like calls to the `print(...)`  function that don't seem like they should denote
any kind of value actually evaluate to `None` implicitly:
```Python
val = print("hello world!")
val is None # evaluates to True
```
(The last line here is an expression, too!)

So then what *isn't* an expression? Well, the simple answer is: everything that
isn't an expression is a *statement* -- pieces of code that *don't* evaluate to
anything (although they may contain other pieces that do), instead referring
only to a set of instructions to pass to the computer. One trivial form of
statement is variable assignment: when you set the value of a variable using
`=`, there's no value returned at the end. As a whole, the `<variable_name> =
<expression>` syntax forms a statement. The first non-trivial example of this
is...

---
<details>
<summary><strong>Check your understanding</strong></summary>

Which of the following are expressions?
1. `4 * "hello world!"`
1. `my_str = "hello world!"[:6] + "Python!"`
1. `print(my_str)`

<details>
<summary>Answer</summary>

The first and third of the above are expressions; the second is not.
</details>
</details>

---

## The `if` statement
The first kind of statement we'll encounter is the `if` statement. As promised,
it's an essential part of how computers are able to make decisions. The concept
behind it is straightforward: It's an instruction for the computer to perform a
set of actions depending on the value of some control expression. The basic
syntax goes like this:
```Python
if expr:
    # execute code here only if expr == True
    print("True!")
```
where `expr` is a *boolean* expression: as its name implies, it's a specific
kind of expression that evaluates to a boolean value (either `True` or `False`).
`else` or `elif` clauses can also be added, which tell the computer what to do
when `expr` evaluates to `False`, or give additional `if` statements that are
considered only when all control expressions before them are `False`:
```python
if expr0:
    # execute only if expr0 == True
    print("expr0 is True!")
elif expr1:
    # execute only if expr0 == False and expr1 == True
    print("expr1 is True!")
elif expr2:
    # execute only if expr0 == False and expr1 == False and expr2 == True
    print("expr2 is True!")
else:
    # execute only if expr0, expr1, and expr2 are all False
    print("expr0, expr1, expr2 are False!")
```
Collectively, everything attached to an initial `if` statement is known as being
part of an `if` "block", in which there can be as many `elif` statements as you
like, but only one `if` and at most one `else`.

Notice in both of these examples how everything in between each `if`, `elif`, or
`else` statement is indented. Aside from making these blocks of code easier to
read, these indentations signal to Python what is part of each clause and what
is not. In Python, the `:` character at the end of a line indicates the
beginning of the block, where every line after the `:` that has the indentation
is considered to be associated with whatever opened the block:
```Python
my_sum = 1 + 1
if my_sum == 2:
    print("Python can do math!") # these lines are inside the if statement
    my_sum + 8 == 10             # they're executed only if my_sum == 2

print("hello world!") # these lines are outside the if statement
my_sum - 2 == 0       # they're executed regardless of whether my_sum == 2 is
                      # True or not
```

---
<details>
<summary><strong>Check your understanding</strong></summary>

Consider the following `if...elif...else` block considering the value of
`testval`:
```Python
testval = ... # some value here

if testval > 0 and testval <= 1:
    print("first")
elif testval > 5 and testval <= 9:
    print("second")
elif testval == "this is a str":
    print("third")
elif testval is None:
    print("fourth")
else:
    print("fifth")
```

What will this program print out for each of the following values of `testval`?
1. `testval = -1`
1. `testval = "this is a str"`
1. `testval = 8`
1. `testval = None`
1. `testval = 3.14159`

<details>
<summary>Answer</summary>

1. `fifth`
1. `third`
1. `first`
1. `fourth`
1. `fifth`

Notice that `second` will never be printed by the program, no matter what the
value of `testval` is!
</details>
</details>

---

## Ternary expressions
As a small footnote, the concept of an `if` statement can be used in another
way, in the form of a *ternary expression*. Looking at the name, we can see
first that this is an expression and not, a statement like the other, main use
of `if` that we saw above, but the concept is very similar in meaning. Second,
the expression is *ter*nary, meaning there are three of something, as opposed to
*bi*nary (two) or *u*nary (one).

To fully illustrate what exactly a ternary expression is, consider the following
common scenario: Suppose you have a variable whose value you want to set to one
of two values, depending on a boolean control expression. You could use the full
syntax for an `if` block with a single `else` clause from the previous
expression to write something like this:
```Python
if control:
    my_variable = expression1
else:
    my_variable = expression0
```
But this is relatively verbose code, considering the simplicity of what you want
to accomplish. Instead, you could use the form of the ternary expression, which
goes like this:
```Python
my_variable = expression1 if control else expression0
```
and just like that, we've condensed four lines down to one! This is a pretty
useful feature (if only to make things a bit more ergonomic for the programmer),
and contributes to Python's reputation as a very *expressive* language: this
single line conveys the programmer's intent quite well, despite having such a
high ratio of compression. This is what's known as *syntactic sugar* -- the
operations it expresses can be accomplished perfectly well in other forms, but
having it in the languages makes things a bit easier to read and write.

Of course, though, the fact that this is an expression means it will always
evaluate to a specific value, and so we can use it in place of other values
outside of simple variable assignment. For example:
```Python
print(expression1 if control else expression0)

x = 5
my_variable = 10 + (6 if x > 2 else -4) + (18 if x <= 12 else 3.854)
```
and so on.

---
<details>
<summary><strong>Check your understanding</strong></summary>

In the last example of the ternary expression above (the one with `my_variable =
...`), what is the value of `my_variable` for `x = 5` as shown? For `x = 1`? For
`x = 15`?

<details>
<summary>Answer</summary>

- For `x = 5`, `my_variable == 34`
- For `x = 1`, `my_variable == 24`
- For `x = 15`, `my_variable == 19.854`
</details>
</details>

---

