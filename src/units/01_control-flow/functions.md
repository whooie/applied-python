# Functions

Functions are *really* important (which you can probably tell by the length of
this page!). As we've mentioned before, functions in programming are essentially
snippets of code that are made portable and reusable: They can be transported
anywhere in a program (or between programs!) and fed different inputs that
affect its ultimate return value. Here, we're going to look at their different
uses as well as some best-practice stuff it's best to form habits for starting
early on.

## Contents
<!-- toc -->

## Basic syntax
At the most basic level, functions are defined using the `def` keyword:
```Python
def my_function(arg):
    # <your code here>
```
where the first line here (containing the function's name and arguments) is
called the function's *signature*, and everything following it is called its
*body*. After this has been relayed to Python, this function can be referred to
as `my_function`, and *called* by passing arguments to it in parentheses, e.g.
`my_function(my_arg)`. Additionally, functions can be defined to take as many
arguments as you want:
```Python
def multiarg_function(arg1, arg2, arg3, ...):
    # <your code here>
```
Finally, these functions can be defined with *return values* that can be stored
in variables using the `return` keyword:
```Python
def add_five(x):
    return x + 5

y = add_five(5) # y == 10
```
and as noted previously, any function that doesn't have an explicit `return`
statement implicitly returns `None`.

---

<details>
<summary><strong>Check your understanding</strong></summary>

1. Write a function, `greet(name)` that takes as input a `str` and returns
   another string that reads `"hello <name>"`, where `<name>` is whatever the
   input string is.

1. Write another function, `repeat(string, n)`, that returns the value of
   `string`, repeated `n` times.

<details>
<summary>Answer</summary>

Here are a couple simple versions of these functions:
```Python
def greet(name):
    return "hello " + name

def repeat(string, n):
    return n * string
```
</details>
</details>

---

## Positional, keyword, and default arguments
First, let's talk about the kinds of arguments that can be passed to a function.
Suppose we have the definition:
```Python
def foo(w, x, y, z):
    return w + 2 * x + 3 * y + 4 * z
```
When we actually call this function later in a program, we can pass values to
the arguments of this function in a couple ways. The first is the simple way,
where we simple replace each function argument with the value we want it to
have, like so:
```Python
foo(1, 5, 4, 3) # returns 35
```
When Python interprets this function call, it assigns values to the variables
`w`, `x`, `y`, and `z` in the function's body according to their positions
relative to each other, and so we say that the values have been passed to the
function as *positional* arguments.

Another way to do this, though, is by referring to the names of these arguments
explicitly:
```Python
foo(w=1, x=5, y=4, z=3)
```
Writing the function call this way passes values to `foo` as *keyword*
arguments, and carries a couple advantages. The first is that, since we're
specifying explicitly which argument each value should be assigned to, we can
switch up their order:
```Python
foo(z=3, w=1, x=5, y=4)
```
The second is that it makes your code more readable. If anyone (including you!)
should have to read your code at a later time, it can help that person to
remember what the arguments of the function are, and (hopefully) understand what
that call is doing, without having to go back to the function's definition. On
the other hand, using all-keyword arguments is quite verbose, and sometimes
unnecessary, depending on the function at hand. (For example, if a function only
takes one argument, it shouldn't be necessary to remind whoever's reading your
code what that argument is named.)

Positional and keyword arguments can also be combined in the same function call:
```Python
foo(1, 5, z=3, y=4)
```
but, critically, all positional arguments must be placed to the left of all
keyword arguments -- otherwise, there can be ambiguity in which argument gets
which value.

The final kind of argument we can use is the *default* argument, which is
specified in a function's signature, rather than its call. Here's the main idea:
Let's say you have a function that has some number of arguments that can affect
its operation. For the most part, though, some subset of arguments control
parameters that the eventual end user who will later be actually using the
function won't really want or need to care about.

For example, take the `print` function. In the vast majority of cases when we
might use `print`, the only arguments we pass to it will simply be those we want
it to print out to the screen, but as we saw in its `help()` info on a previous
page, it actually has four arguments, `sep`, `end`, `file`, and `flush` that one
can specify by keyword argument. This raises the question: how is it possible
for us to have avoided passing values to these arguments every time we've called
`print` up until now?

The answer is, of course, that these arguments are *default* arguments, which is
shown in its `help()` info, re-printed here for convenience:
```
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)

    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  a file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default a space.
    end:   string appended after the last value, default a newline.
    flush: whether to forcibly flush the stream.
```
Notice how, in the function's signature, these arguments are specified with an
`=` sign, followed by some kind of value. This syntax indicates to Python that
it can assume a certain *default* value for the arguments if they happen to be
left unspecified in a particular call. All you have to do to add defaults to
your own functions is use this same syntax. For example,
```Python
def function_with_defaults(arg1=3, arg2=18, arg3=5):
    return arg1 * arg3 + arg2

function_with_defaults() # returns 33
function_with_defaults(10) # returns 68
function_with_defaults(arg2=7) # returns 22
```
and one thing to note is that, for the same reason that all keyword arguments
passed to a function have to be passed after all positional arguments, all
default arguments in a function's definition must come after all of its
non-default arguments.

---

<details>
<summary><strong>Check your understanding</strong></summary>

Play around a bit with `print()`'s different keyword arguments. What happens
when you change the value of the `sep` and `end` keyword arguments?

`'\n'` is the default value of `end` (representing a "newline" character); what
happens when you change it to `'\r'` ("carriage return") or `'\b'`
("backspace")? Make sure you have multiple `print()` statements in your test
programs to check these.

Then what happens when you change the value of `sep` while printing multiple
items?
</details>

---

## Passing arguments programmatically
Before moving on, let's take a look at how all of this argument-passing can be
done *programmatically* -- although you'll probably be passing individual
arguments to functions explicitly and by hand most of the time, it can also be
useful to delegate this to the program and generalize your functions' behaviors
to a wider range of use cases. Python is actually a bit of a special case in
this regard. Where other languages may restrict you to a certain number of
arguments or only certain types for each argument, Python does neither and
simply ferries values into and out of function bodies. To this purpose, it
provides frameworks for both accepting and passing potentially unlimited numbers
of arguments to any function, given some special syntax.

### Unpacking operators
First, we'll look at how to *pass* arguments programmatically through
*unpacking*. Consider the following from last unit's exercises. Suppose you have
a function to calculate the Euclidean distance between two points with
coordinates $(x_1,\, y_1)$ and $(x_2,\, y_2)$:
```Python
from math import sqrt

def dist_euc(x1, y1, x2, y2):
    return sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
```
and are given your points as `tuple`s:
```Python
P1 = (3, 4)
P2 = (12, 5)
```

One way to pass this information to the `dist_euc(...)` is, of course, by
accessing the individual elements of your `tuple`s:
```Python
dist_euc(P1[0], P1[1], P2[0], P2[1])
```
But this is cumbersome to type out. Instead, we can simply *unpack* our `tuple`s
into the function using the `*` unpacking operator, as in
```Python
dist_euc(*P1, *P2)
```
This is much more concise and more easily conveys what your code is trying to
do! So what's going on here? For any *iterable* object -- essentially, any
object you can loop over with `for x in <iterable>:` (really, this means that
the object implements a special method that we'll talk about more in the next
unit) -- we can use `*` as a *unary* operator (as opposed to a *binary*
operator, like in multiplication) to expand the elements of the object out into
positional arguments in a function.

For the specific case of the above, you can think of this operator as performing
```
*P1 -> 3, 4
*P2 -> 12, 5
```
in the function's arguments. Note the order, however -- these expansions are as
*positional* arguments, which means that if `dist_euc` were instead defined as
```Python
def dist_euc(x1, x2, y1, y2):
    return sqrt( (x2 - x1)**2 + (y2 - y1)**2 )
```
This unpacking operation would not give the desired outcome.

We can also unpack iterables when creating other iterables. For example,
```Python
(*P1, *P2) == (3, 4, 12, 5) # unpack into a tuple
[*P1, *P2] == [3, 4, 12, 5] # unpack into a list
{*P1, *P2} == {3, 4, 12, 5} # unpack into a set
```

So then how can we unpack into *keyword* arguments? The answer is the other
unpacking operator, `**`. As suggested, this unpacks the elements of any object
that behaves like a mapping -- again, we'll get into what exactly this means in
the next unit; usually this just means `dict`s -- into keyword arguments when
done for a function. For example:
```Python
my_kwargs = {"x1": P1[0], "x2": P2[0], "y1": P1[1], "y2": P2[1]}

dist_euc(**my_kwargs)
```
where we can take the action of this operator to be
```
**{"x1": P1[0], "x2": P2[0], "y1": P1[1], "y2": P2[1]}
    -> x1=P1[0], x2=P2[0], y1=P1[1], y2=P2[1]
```

Similar to the other unpacking operator, `**` can also be used in the creation
of other mappings:
```Python
P1_kw = {"x1": P1[0], "y1": P1[1]}
P2_kw = {"x2": P2[0], "y2": P2[1]}

P12_kw = {**P1_kw, **P2_kw} # dict is the only built-in mapping
```

This may not seem very useful right now, but for a sufficiently complex program
where the arguments you may wish to pass to a function need to be assembled from
a variety of other parts of the program (or perhaps from the contents of a file
-- most configuration file-reading libraries return the contents of those files
as `dict`s), this can be very ergonomic!

### Wildcard arguments
Now we'll look at *wildcard* arguments, which allow functions to *accept*
arguments programmatically -- phrased another way, it allows functions to take a
*variable* number of arguments without having to define defaults for everything,
or enable the function to safely accept unexpected arguments. Here, the syntax
is intentionally reminiscent of the two unpacking operators we discussed above:
```Python
def wildcard_func(*args, **kwargs):
    ...
```
Here, this function can accept any number of arguments, both positional and
keyword. For example,
```Python
wildcard_func(
    1, 8.8, 1.0j, [True, True, False], range(10), "hello",
    kwarg1="wheeee", kwarg2="eeeeee",kwarg3="eeee!",
)
```

Once accepted by the function, all positional arguments are then packed into a
`tuple` and all keyword arguments are packed into a `dict` with `str`s as keys,
called `args` and `kwargs`, respectively:
```Python
def wildcard_types(*args, **kwargs):
    print(type(args))
    print(type(kwargs))

wildcard_types()
# prints out:
# <class 'tuple'>
# <class 'dict'>
```
and can then be treated as such. To access the contents of these objects (the
arguments themselves), all one needs to do is provide indices to `args` or `str`
keys to `kwargs`.

One convenient example of a function that uses this behavior is the `print()`
function. From it's `help()` info, we have its signature as
```Python
print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
```
But a better way to look at this is perhaps
```Python
print(*values, sep=' ', end='\n', file=sys.stdout, flush=False)
```

---

<details>
<summary><strong>Check your understanding</strong></summary>

Wildcard arguments are a very powerful tool when combined with the looping
techniques discussed on the previous page. Write a function that takes a
wildcard positional argument, and returns all of the items passed to it as
strings concatenated together.

Now create a `list` of the squares of the first ten natural numbers (the squares
of 1, 2, ..., 10), unpack it into your function, and print out the result.

<details>
<summary>Answer</summary>

Here's a simple implementation:
```Python
def concatenate_as_str(*things):
    acc = "" # initialize an accumulator string
    for thing in thing:
        acc += str(thing)
    return acc

squares = [k**2 for k in range(1, 11)] # ideal use case for list comprehension!

print(concatenate_as_str(*squares))
# prints out '149162536496481100'
```
</details>
</details>

---

## Returning and unpacking values
Now we'll talk a bit about how to interact with functions in the opposite way,
which is through their *returned* values. As we've seen, functions can output
values from their bodies via the `return` keyword. From a technical standpoint,
every function has a return value -- even in the case that `return` is not used
anywhere in a function's body, the function will still implicitly return `None`.

Upon encountering a `return` statement, a function will immediately halt is
execution, breaking out of all loops and if statements. For example:
```Python
def return_test():
    for k in range(100):
        return k
    print("reached end of loop")

return_test() # returns 0 and does not reach the call to `print`
```
Further, these `return` statements also don't need to include a value; in this
case, they'll immediately halt execution of the function and implicitly return
-- you guessed it -- `None`.

One other thing to note is that `return` statements can involve multiple values:
To do this, the syntax is
```Python
def multi_return(x, y):
    return x + 5, y * 7
```
where each value to be returned is separated by a comma. Upon encountering this,
the function will actually pack each value to be returned into a `tuple` and
return that instead. At this point you could say that, technically, functions
can actually only return one value, since
```Python
type(multi_return(4, 5)) == tuple # evaluates to True
```
-- in fact, you can also see that any sequence of comma-separated values (with
or without parentheses) is considered a `tuple` by default, as in
```Python
x = 4, 5
type(x) == tuple # evaluates to True
```
-- but usually the distinction between returning a `tuple` and returning
multiple values is made because of the way that `tuple`s and other iterable
objects can be unpacked by variable assignment.

To make this more concrete, we'll first look at this in action, using the output
of `multi_return(...)`. As we've noted, the values returned by this function are
packed into a `tuple`:
```Python
P = multi_return(4, 5)
type(P) == tuple # evaluates to True
```
If we wanted to then let two other variables represent the two elements of this
`tuple`, we'd normally then write something like this:
```Python
a = P[0]
b = P[1]
```
but a special syntax can skip over the intermediary step of assigning to `P`:
```Python
a, b = multi_return(4, 5)
```
where we say that the values returned by `multi_return` are then "unpacked" into
the variables `a` and `b`. Another general term for this is "destructuring
assignment," since the "structure" of the `tuple` is removed by the act of
assigning values to the variables `a` and `b`.

This behavior holds for any number of arguments as well as any kind of iterable
object, and can even be nested:
```Python
pi, e, phi = [3.142, 2.718, 1.618]
P1, P2 = [(3, 4), (12, 5)]
(x1, y1), (x2, y2) = [(3, 4), (12, 5)]
```
Assigning variables this way is often especially useful in loops or functional
programming contexts (again, next unit). For example, if we want to iterate over
the pairs of a `dict` or the elements of a `list` along with their indices, we
can use the following:
```Python
my_dict = {"a": 1, "b": 2, "c": 3}
# .items() iterates over (key, value) tuples
for key, value in my_dict.items():
    print(key, "->", value)

my_list = ["a", "b", "c"]
# enumerate(my_list) iterates over (index, value) tuples
for index, char in enumerate(my_list):
    print(index * char)
```
Unpacking the iterable objects into the loop variables is a great way to make
your code more legible to readers, via more descriptive variable names.

---

<details>
<summary><strong>Check your understanding</strong></summary>

In addition to `enumerate()` mentioned above, `zip()` is another useful tool for
packing iterable objects together. Namely, it takes any number of iterable
objects -- `list`s, `tuple`s, whatever -- and packs all the elements at
corresponding indices in each iterable object together into tuples. This is very
useful in its own right, but here gives us another opportunity to practice
unpacking these `tuple`s in a loop.

Write a piece of code that takes individual `list`s of $x$-, $y$-, and
$z$-coordinate values
```Python
x_coords = [2, 3, 1, 1, 4, 5, 7, 6, 2, 9, 10]
y_coords = [8, 3, 2, 2, 1, 4, 5, 2, 3, 4, 4]
z_coords = [4, 6, 1, 2, 8, 5, 3, 4, 7, 6, 2]
```
(corresponding to ten total points in space) and constructs a fourth `list` of
only the sets of coordinates (packed together in in 3-`tuple`s) whose distance
from the origin is less than 10.

<details>
<summary>Answer</summary>

The `zip()` function makes this pretty easy!
```Python
from math import sqrt

less_than_10 = list() # initialize our accumulator

# here's our destructuring assignment
for x, y, z in zip(x_coords, y_coords, z_coords):
    if sqrt(x**2 + y**2 + z**2) < 10:
        less_than_10.append((x, y, z))
```
Bonus question -- how could you rewrite this as a list comprehension?
</details>
</details>

---

## Scopes and mutability
Switching gears a bit, we can now talk about how *scopes* work in Python. First
off, a "scope" in programming refers to the part(s) of a program where a given
variable is considered a valid name to refer to whatever value it's bound to.
Python is often somewhat loose with its scopes: In many languages, `for` and
`while` loops usually open their own scopes for their operations, and then close
them once the loop has finished executing -- any values assigned to variables
inside the scope of the loop then cannot be accessed by those names from the
outside.

This is not the case in Python. For example,
```Python
k = "hello"
for k in range(100): # use an externally assigned variable for the loop
    if k == 50:
        x = k**2 # assign to x inside the loop

k == 99 # evaluates to True
x == 2500 # evaluates to True
```
where we can see both that variables assigned to *outside* of the loop (`k`) can
be reassigned to *inside* it, as well as that variables assigned to *inside* the
loop still retain their values when accessed from the *outside*.

Here, we'll contrast this behavior with that for functions, which is a case
where Python maintains harder scope boundaries.

### Scope boundaries
The key distinction to make here is between *local* variables (those only
accessible from within a given scope) and *global* variables (those also
accessible from others), plus those lying in a kind of middle ground between the
two. In Python, the default is to have all variables from *surrounding* scopes
be readable, but not necessarily mutable by direct assignment -- that is, the
variables' values can be used and referred to, but not reassigned. To
illustrate, we can consider the following structure, where each scope ($A$
through $E$) can be thought of as a set of all its variable definitions:

<center>
<img src=assets/functions/scopes.svg width=65%>
</center>

The scopes whose variables are readable to each of the scopes $\{A,\, B,\, C,\,
D,\, E\}$ are ones that contain, but are not contained by the scope:

| Scope | Readable scopes |
|:------|:----------------|
| $A$   | $A$             |
| $B$   | $A,\, B$        |
| $C$   | $A,\, B,\, C$   |
| $D$   | $A,\, D$        |
| $E$   | $E$             |

Alternatively, this can be thought of as a kind of tree, where each scope
*inherits* the variable definitions of all those that are placed above it in the
tree:

<center>
<img src=assets/functions/scopes-tree.svg width=35%>
</center>

So then what constitutes a scope? We'll mainly focus on functions for this page,
but other examples of when distinct scopes are opened are *modules*, which we'll
talk more about on the [program design][program-design] page, and *classes*,
which we'll talk about on the [object-oriented programming][object-oriented]
page.

### Local versus global
Now we can make the distinctions between "local," "global," and that dubious
middle ground we mentioned before a bit more clear through a concrete example.
Let's consider a short program in which we have two distinct scopes:
```Python
my_number = 5
my_list = [0]

def foo():
    print("my_number in foo =", my_number)
    print("my_list in foo =", my_list)
    my_list.append(15)

def bar():
    my_number = 10
    my_list = ["a"]
    my_list.append("b")

    print("my_number in bar =", my_number)
    print("my_list in bar =", my_list)

foo()
bar()
print("my_number =", my_number)
print("my_list =", my_list)
```
Without running any of this code, take a second to think through what's
happening here and form a firm guess for what will be printed out by the end.
Generally, we want to consider three rules:
- Anywhere a variable can be assigned constitutes a scope.
- From within a given scope, the values assigned to variables in any
  encompassing scope can be read with all their attributes, excluding methods;  
  the variables themselves cannot be reassigned.
- When a variable is used in an expression, its value is determined by ascending
  through the relevant tree of scopes (starting at the scope immediately
  containing the expression) and searching each one for a definition. If none
  are found, then an error is thrown. If a variable whose name coincides with
  that of another in a surrounding scope is assigned a value, then this newest
  definition takes precedence over all others for every expression in the same
  scope.

With these in mind, we can think through the program above and reason that it
will print out:

---
<details>
<summary>Answer</summary>

```
my_number in foo = 5
my_list in foo = [0]
my_number in bar = 10
my_list in bar = ['a', 'b']
my_number = 5
my_list = [0, 15]
```

Note that, in `foo()`, even though we can't directly reassign `my_list`, we
can still mutate it through its methods!
</details>

---

There are two important things to note here:
1. By default, variables from surrounding scopes can only be referenced if they
  are not assigned anywhere else in the immediate scope, even if that assignment
  happens after the reference. For example, the following will throw an error:
    ```Python
    my_number = 5

    def foo():
        print(my_number) # attempt to reference my_number from outside foo
        my_number = 10 # define a version of my_number that is specific to foo

    foo()
    ```
1. Although we talk about scopes in terms of which ones contain others, this
  concept of "containment" refers only to how things are *defined*, and not to
  how they may be *executed*. For example, we can define two functions with one
  calling the other in its body:
    ```Python
    def func_a():
        # <do some stuff>
        ...

    def func_b():
        # <do some stuff>
        b = 500
        func_a()
    ```
  Even though `func_b()` calls `func_a()` during its execution, their respective
  scopes are not linked -- `func_a()`'s scope cannot read the value of `b` as
  it's defined in `func_b()`'s.

Despite all that we've said about rules for referencing values defined in other
scopes, Python provides a way to escape this system entirely through the
`global` keyword. In short, declaring a variable to be "global" (through the
syntax `global <my_variable1>, <my_variable2>, ...`) tells Python to find the
variable's definition occurring in the outermost surrounding scope (if one
exists) and use literally that instance of the variable for all following
operations, including value reassignment. Consider the following example:
```Python
# outermost scope
my_number = 1

def foo(): # sub-scope
    my_number = 2

    def bar(): # sub-sub-scope
        global my_number # refer to the instance of my_number outside foo
        print(my_number) # prints out 1

        my_number = 3

    bar()
    print(my_number) # this instance is not affected! (prints out 2)

foo()
print(my_number) # prints out 3
```
Note that when a variable is declared using `global`, *only* the outermost
scope's definitions are affected (and only if a relevant definition already
exists there when `global` is used)!

So now we can offer definitions for what "local" variables, "global" variables,
and those in the middle are, with respect to a given scope $S$:
- *local* variables are ones that are defined in $S$. These variables can be
  read from any scopes that $S$ contains, but not reassigned unless `global`  
  is used.
- *global* variables are those defined in some scope containing $S$ and pulled
  into $S$ by `global`. These variables can be reassigned at will in $S$, and
  these reassignments affect the value of the variable in the outer scope as
  well.
- Variables that are *neither* "local" nor "global" are those defined in scopes
  outside $S$, but not referenced by $S$ using `global`. These variables' values
  can be read or mutated via methods or other functions from within $S$, but not
  through reassignment.

Generally, though, the use of `global` variables is considered bad practice and
should be avoided.

### Mutability considerations for default arguments
We'll finish this section out with a brief note on an important consideration to
make when using mutable container types as default arguments. As we saw above,
you can set the values of default arguments for a given function in its
definition and, in principle, these values can be whatever you want. Then these
values are bound to variables in the scope of the function's body, and can be
interacted with at will. As a toy example, take the following function:
```Python
def append_to(item, some_list=list()):
    some_list.append(item)
    return some_list
```
and call it a few times to test it out:
```Python
print(append_to("one"))
print(append_to("two"))
print(append_to("three"))
```

You probably expect these function calls to print out something like this:
```
['one']
['two']
['three']
```
But if you actually run this code, what you'll really see is:
```
['one']
['one', 'two']
['one', 'two', 'three']
```
So what's going on here? The answer is related to how scope boundaries are only
related to definitions, and not executions: The default value,
`some_list=list()`, is evaluated and bound to the variable `some_list` in the
scope of `append_to(...)` *only when Python first encounters these definitions,
not each time `append_to(...)` is called*. This is true of every default
argument, but is especially troublesome for mutable types.

To remedy this, you can take advantage of the `None` type, and use it to set the
default value in the bodies of your functions instead, as in:
```Python
def append_to(item, some_list=None):
    some_list = list() if some_list is None else some_list
    some_list.append(item)
    return some_list
```
With this definition, now our function works as expected!

---

<details>
<summary><strong>Check your understanding</strong></summary>

Given the following piece of code, find the number of distinct scopes. Then,
without running any code, determine which functions will modify values outside
their scopes and which will result in a scoping error when called.
```Python
my_numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

def func1():
    my_numbers = []
    for k in range(1, 11):
        my_numbers.append(k)

def func2():
    global my_numbers
    my_numbers.append(21)

def func3(my_numbers):
    my_numbers += [42, 144]

def func4(my_numbers):
    my_numbers = my_numbers + [42, 144]

def func5():
    my_numbers.append(5)
    my_numbers = [k**2 for k in range(10)]

def func6():
    my_numbers.append(5)

def func7(N):
    my_numbers = []

    def func8(X):
        my_numbers.append(X)

    for k in range(N):
        func8(str(k))
```

<details>
<summary>Answer</summary>

There are nine distinct scopes defined here. The first is the default scope,
where `my_numbers` is equal to `[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]`. Then there are
eight more (one for each function) with the scope for `func8` being contained
within the scope for `func7`. Call them scopes 0, ..., 8. Going through each
function, we have:
- `func1`: Defines `my_numbers`, which is local to scope 1. When it is modified
  by `.append`, none of these mutations propagate back to the `my_numbers` in
  scope 0.

- `func2`: Explicitly declares `my_numbers` in its scope as `global` so it
  refers to the original `my_numbers` defined in scope 0. When
  `my_numbers.append(21)` is called, `my_numbers` in scope 0 is mutated.

- `func3`: All of a function's arguments are defined as variables local to its
  scope, so in this case `my_numbers` refers to an object that is passed to
  `func3` using this argument and not necessarily the `my_numbers` in scope 0.
  However, `my_numbers` can still refer to objects outside of `func3` when they
  are explicitly passed as argument; for instance, the following code defines a
  list and mutates it using `func3`:
  ```Python
  some_numbers = [1, 2, 3] # define a list outside of func3
  func3(some_numbers) # pass it to func3
  print(some_numbers) # now some_numbers == [1, 2, 3, 42, 144]
  ```

- `func4`: Like `func3`, this function can refer to values from outside its
  scope if they are passed by argument. But in this case, the variable name
  `my_numbers` (which is local to `func4`) is immediately reassigned to the
  value of the expression `my_numbers + [42, 144]`. In this expression,
  `my_numbers` refers to the argument `my_numbers` (before the name is
  reassigned), and the operator `+` concatenates it with the new list `[42,
  144]`. Importantly, this operation creates another new list whose value is
  `[<values of my_numbers>, 42, 144]` and to which `my_numbers` is then
  reassigned. Since the `+` operation creates a new list, neither the
  `my_numbers` from scope 0 nor the value passed to `func4` are mutated:
  ```Python
  some_numbers = [1, 2, 3] # define a list outside of func4
  func4(some_numbers) # pass it to func4
  print(some_numbers) # no mutation occurs here; some_numbers == [1, 2, 3]
  ```

- `func5`: This is an interesting case. If this function is read line-by-line,
  it appears fine -- in the first line, `my_numbers` refers to the definition in
  scope 0 and in the second line, this name is redefined in the local scope to
  refer to a new value. But due to technical considerations, Python detects the
  first line as one that tries to refer to a variable that hasn't been assigned
  a value yet. As a result, it throws a `UnboundLocalError`.

- `func6`: This function is identical to `func5`, but without the second line
  attempting to assign a value to `my_number` in the local scope. Since this was
  the key issue in `func5`, no error is thrown and the function behaves as
  expected -- `my_numbers` from scope 0 is mutated.

- `func7`/`func8`: The first line of `func7` defines a local variable
  `my_numbers`, so right away we know that the `my_numbers` from scope 0 is
  irrelevant. Then `func8` is defined within `func7`, which means that, absent
  any use of `global`, scope 7 is entirely within scope 8. As such, the value of
  `my_numbers` referenced inside `func8` refers to (and mutates) the
  `my_numbers` from the first line of `func7` when it's used later in the `for`
  loop to append strings.
</details>
</details>

---

## Higher-order functions and `lambda`
Following the topic of what sorts of things can be passed to functions, now is a
good time to mention that in Python, functions are actually still considered
objects, just like any other. This means they can be assigned to variables and
passed as arguments to or returned as results from other functions -- functions
that do one or both of these are called *higher-order* functions.

For example, we might write a higher-order function that automates the process
of finding the mean of a function defined over a set of values for an
independent variable:
```Python
def mean(func, xvals):
    yvals = [func(x) for x in xvals]
    acc = 0.0
    for y in xvals:
        acc = acc + y
    return acc / len(yvals)

# define two functions that we'd like to calculate the mean of
def square(x):
    return x**2

def cube(x):
    return x**3

# define our domain
xvals = [x for x in range(-5, 6)]

# now we can find the means of these functions over the domain in a concise way
square_mean = mean(square, xvals)
cube_mean = mean(cube, xvals)
```
Or we might want to write a function that takes the coefficients of a line of
the form $y = a x + b$, and returns another function that calculates $y$ for
arbitrary $x$:
```Python
def make_line_func(a, b):
    def line_func(x):
        return a * x + b
    return line_func

# makes a function describing a line with slope 5.5 and y-intercept 2.1
my_line1 = make_line_func(5.5, 2.1)

# makes a function describing a line with slope 3.2 and y-intercept 8.0
my_line2 = make_line_func(3.2, 8.0)
```

In many cases, you may wish to use higher-order functions with very simple other
functions (ones that are perhaps closer to the more math-y kind we mentioned on
the page about [program input/output][program-io]), or maybe one that will only
be used once. In both of these cases, the full function definition syntax that
we've been using so far becomes a bit cumbersome to work with. Here, we can use
a special kind of function called a *lambda* function, which is *anonymous* (it
has no name unless specifically assigned to a variable) and features a
simplified syntax based around substitution of values into a single expression.

The syntax goes like this:
```Python
my_lambda_func = lambda arg1, arg2, ...: <return expr>
```
where, as shown, lambda functions are declared using the `lambda` keyword and
can take multiple arguments. Importantly, however, the "bodies" of lambda
functions must consist only of a single expression that is evaluated and
returned when the lambda function is called. Lambda functions also obey the same
scoping rules as regular functions.

In the above examples, we can replace the simple functions (`square`, `cube`,
and `line_func`) with lambda functions like so:
```Python
def mean(func, xvals):
    yvals = [func(x) for x in xvals]
    acc = 0.0
    for y in yvals:
        acc = acc + y
    return acc / len(yvals)

xvals = [x for x in range(-5, 6)]

# we can create the lambda functions right inside the calls to `mean` thanks to
# the anonymity of the syntax
square_mean = mean(lambda x: x**2, xvals)
cube_mean = mean(lambda x: x**3, xvals)
```
```Python
def make_line_func(a, b):
    return lambda x: a * x + b

my_line1 = make_line_func(5.5, 2.1)
my_line2 = make_line_func(3.2, 8.0)
```
and if we really wanted to, we could have `make_line_func(...)` also be a lambda
function that itself returns a lambda function:
```Python
make_line_func = lambda a, b: lambda x: a * x + b

my_line = make_line_func(5, 1)

my_line(3) == make_line_func(5, 1)(3) # evaluates to True
```

Lambda functions are often quite convenient to use in the right contexts, such
as when using Python for [functional programming][functional].

---

<details>
<summary><strong>Check your understanding</strong></summary>

1. Write a higher-order function that takes as input another function and a list
   of values, and calls the other function on each of those values, returning
   them in another list.

1. Write another higher-order function that takes as input some number of
   coefficients for a polynomial (a function of the form $a x^0 + b x + c x^2 +
   \cdots + z x^N$) and returns another function that computes values of the
   actual polynomial. Your function should have the first argument correspond
   to the coefficient on the $x^0$ term, with the total polynomial having terms
   of maximum power $N$, where $N + 1$ is the total number of coefficients
   passed to the function. You may find the `sum` and `enumerate` functions
   (check out their `help` pages!) to be useful.
<details>
<summary>Answer</summary>

1. This kind of higher-order function is generally called something like a
   "mapping" function, since it just takes a list of inputs and directly maps
   them to their outputs in a simple kind of way.
   ```Python
   def map(func, input_values):
       return [func(x) for x in input_values]
   ```

1. This is a great case to use a wildcard positional argument! We'll take a
   variable number of coefficients and map each one to a term in the polynomial.
   The `sum` function is particularly helpful for this, since we can calculate
   each of the terms individually, pack them all into a list, and then just call
   `sum` on the list at the end. `enumerate` is also useful, since it allows us
   easily keep track of the power each term:
   ```Python
   def make_polynomial(*coeffs):
       return lambda x: sum([c * x**k for k, c in enumerate(coeffs)])
   ```
   A more elementary implementation goes like this:
   ```Python
   def make_polynomial(*coeffs):
       def polynomial(x):
           k = 0
           acc = 0
           for c in coeffs:
               acc += c * x**k
               k += 1
           return acc
      return polynomial
   ```
</details>
</details>

---

## Recursion
Now we'll talk briefly about a different way to construct and use functions,
which is by leveraging something called *recursion*. Simply put, recursion in
this context, is when a function calls itself in its own body. Somewhat
cheekily, we might say -- to understand recursion, one first needs to understand
recursion.

To take a more productive tack, we can draw an analogy to inductive proofs. In a
proof by induction, the goal is to prove some proposition $P(n)$ (where $n =
0,\, 1,\, 2,\, \dots$ or some enumerated equivalent) by starting with a *base
case* -- usually $n_0 = 0$ or $n_0 = 1$ -- for which $P(n_0)$ is true, and then
establishing that the truth of any $P(k)$ implies the truth of $P(k + 1)$. In
this way, the truth of any $P(n \geq n_0)$ can then be seen to ultimately be
implied by the truth of the base case:
$$
P(n_0)
    \implies P(n_0 + 1)
    \implies P(n_0 + 2)
    \implies \cdots
    \implies P(n)
$$
(for an example proof by induction, see the [Wikipedia
page][induction-example].)

A recursive function shares this general structure, but from the opposite
direction: to evaluate a recursive function $f$ called on some particular input
$x_n$, we need to first evaluate $f(x_{n - 1})$, the value of which then depends
on $f(x_{n - 2})$, which depends on $f(x_{n - 3})$, (...) all the way down to
the base case $f(x_0)$, which hopefully yields a concrete value rather than
depending on another recursive call of $f$. When this base case is reached,
every other function call in the *call stack* can then be evaluated to other
concrete values, and we finally get a result for $f(x_n)$. Note here that the
intermediary values $x_k$ don't necessarily have to be numbers, so long as they
eventually reach the base case -- it's very important to have a base case for
your recursive functions, otherwise you reach a state of infinite recursion
that, like infinite loops, effectively freeze your program.

A quintessential example of a recursive function is the $(\,\cdot\,)!$ factorial
function:
$$
n!
    = \begin{dcases}
        1
            & \t{if}~ n \leq 0
        \\
        n \times (n - 1)!
            & \t{otherwise}
    \end{dcases}
$$
With this definition, we can evaluate $5!$ in the following way:
$$
\begin{aligned}
    5!
        &= 5 \times 4!
        \\
        &= 5 \times 4 \times 3!
        \\
        &= 5 \times 4 \times 3 \times 2!
        \\
        &= 5 \times 4 \times 3 \times 2 \times 1!
        \\
        &= 5 \times 4 \times 3 \times 2 \times 1 \times 0!
        \\
        &= 5 \times 4 \times 3 \times 2 \times 1 \times 1 = 125
\end{aligned}
$$
In Python, we'd write this function like so:
```Python
def factorial(n):
    if n <= 0:
        return 1
    else:
        return n * factorial(n - 1)
```
Here, `n <= 0` is our base case (where we return 1), and for all others we
return `n * factorial(n - 1)`. So for something like `factorial(5)`, we have to
descent through a series of recursive calls to `factorial(...)` until we get to
`factorial(0)`, at which point we can actually do all the multiplication and get
our answer.

Recursive functions are often quite useful and elegant solutions for dealing
with repetitive operations on possibly infinite tree-like structures or other
scenarios where you don't know (or it would be costly to figure out) exactly how
many of those operations need to be done in a way that would make a using a loop
more appealing.

---

<details>
<summary><strong>Check your understanding</strong></summary>

A common way to store certain kinds of data is through a [tree-like
structure][trees], which can be implemented in Python as nested dictionaries. In
this implementation, the keys of each dictionary correspond to distinct nodes in
the tree: If a particular node is a leaf, that key maps to a particular value,
otherwise it maps to another dictionary. The root node is then represented as
the outermost dictionary. Here's an example:
```Python
# the following nested-dictionary structure encodes this tree:
#                   root
#                  / |  \
#             node1  |   node3
#            /      node2  |  \
# node4 = "a"      /  |    |   node5
#        node6 = 42   |    |        \
#          node7 = None    |         node8 = [1, 2, 3]
#                          node9 = "leaf!"
root = {
    "node1": {
        "node4": "a",
    },
    "node2": {
        "node6": 42,
        "node7": None,
    },
    "node3": {
        "node5": {
            "node8": [1, 2, 3],
        },
        "node9": "leaf!",
    },
}
```
Given this kind of structure, it's often useful to write some kind of function
that can access a series of nodes, or "traverse" the graph. Write a recursive
function that takes a tree defined in this way and a list containing keys that
correspond to a path through the tree, starting at (but not including) the root
node, and return what lies at the end of the path. This can be either a certain
value if the end of the path is a leaf, or a sub-tree if the end of the path is
a branch. For example, your function should have the following behavior for the
above definition:
```Python
def tree_traverse(tree, path):
    # your implementation here
    ...

tree_traverse(root, ["node1", "node4"]) == "a"
tree_traverse(root, ["node2"]) == {"node6": 42, "node7": None}
```
<details>
<summary>Answer</summary>

When writing recursive functions, you should always first identify the base and
recursive cases, and how to test for them. In this case, the strategy will be to
test on the number of items remaining in the path: If only one remains (the base
case), simply return the result of indexing the given dictionary with that item,
otherwise (the recursive case) pass the indexed result to a recursive call with
all of the path items after the one that was used:
```Python
def tree_traverse(tree, path):
    # `tree` is a dict, `path` is a list

    # account for the empty-path case by doing nothing
    if len(path) == 0:
        return tree

    # if only one item in the path, just index the dict
    elif len(path) == 1:
        return tree[path[0]]

    # recursive call: the path is shortened by one to progress to the base case
    else:
        return tree_traverse(tree[path[0]], path[1:])
```
</details>
</details>

---

## Docstrings and annotations
Finally, we'll end with a couple notes on best practices for documenting the
functions you create. It's often the case that we'd like to specify constraints
on the types or possible values that the inputs to a function can or should
have, or what those of the corresponding outputs will have. Since Python is
dynamically typed (there's no built-in way to limit which types that can be
passed to a function) and interpreted (there's no compilation step where things
can be checked before the program is actually run), the only remaining way to
specify these constraints is to whoever is using the function.

To avoid requiring developers to look at the full bodies of functions or
comments in the source code, Python implements two ways to write and read notes
of this kind. The first way is through what are called *docstrings*. When Python
is reading through a file and encounters a function definition, it takes the
first string placed right after the function's signature that is not assigned to
a variable as a kind of metadata, in which the author of the function can
describe things like:
- What the function does
- What sorts of inputs it expects
- What sorts of outputs it returns
- Any special behaviors or warnings that people using the function should take
  note of

A good docstring will include enough information in all of these areas that
anyone who wants to use the function will not be required to know anything about
what goes on inside it.

Generally, docstrings are given with a specially denoted string that uses
triple-quotes (`"""` or `'''`) on either side that preserve line-endings, but
any string fitting the description above will work. For example, we can add a
docstring to our `factorial` function:
```Python
def factorial(n):
    """
    Computes the factorial of a given integer `n`. Defined such that the
    factorial of any number less than 1 is 1.

    Parameters
    ----------
    n : int
        Number whose factorial is to be computed.

    Returns
    -------
    n! : int
        Factorial of `n`.
    """
    if n <= 0:
        return 1
    else:
        return n * factorial(n - 1)
```
and note that, while there is technically notion stopping someone from passing a
`float`, `complex`, or any other weird type to `factorial`, the docstring at
least specifies what the function *expects* its input to be.

Once a function's docstring is defined, it is stored as an attribute of the
function object, and displayed along with the function's signature in its
`help()` information. If we then called `help(factorial)`, we'd see something
like
```
Help on function factorial in module __main__:

factorial(n)
    Computes the factorial of a given integer `n`. Defined such that the
    factorial of any number less than 1 is 1.

    Parameters
    ----------
    n : int
        Number whose factorial is to be computed.

    Returns
    -------
    n! : int
        Factorial of `n`.
```

The second way to include extra information like this in a function's definition
is through *type annotations*. In a function's signature, these annotations
refer specifically to the types that arguments and/or return values are expected
to be, and follow the syntax:
```Python
def my_func(arg: <type>, def_arg: <type>=<default_value>, ...) -> <type>:
    ...
```
where `<type>` can be any object type like `int`, `float`, `list`, etc.
Annotations can also include a body surrounded by brackets -- useful for
container types, e.g. `list[int]` -- to include even more information or place a
`|` character between multiple types to indicate that something can be one of
the those specified, though these may require an extra line `from __future__
import annotations` placed at the top of your program, depending on the version
of Python in use. These annotations will be included in the function's signature
printed in its `help()` information.

Both docstrings and type annotations are purely aesthetic features (they do
nothing to actually limit or constrain what can be passed to or returned from a
function), but can go a long way towards making your code usable by others -- or
even yourself -- and are included in every well-written program.

---

<details>
<summary><strong>Check your understanding</strong></summary>

Add docstrings and type annotations to all of the regular functions (not the
lambda functions) that you wrote for the other **Check your understanding**s on
this page. Be sure to look at their `help` pages!
</details>

---

[program-design]: ../03_basic-scripting-patterns/program-design.md
[object-oriented]: ../02_paradigms-and-data-structures/object-oriented.md
[program-io]: ../00_types-and-input-output/program-io.md
[functional]: ../02_paradigms-and-data-structures/functional.md
[induction-example]: https://en.wikipedia.org/wiki/Mathematical_induction#Sum_of_consecutive_natural_numbers
[trees]: https://en.wikipedia.org/wiki/Tree_(graph_theory)
