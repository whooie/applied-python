# Automated iteration

Now we can talk about two other kinds of statements, which instruct the computer
to perform many operations in rapid succession through iteration or, a bit more
colloquially, *loops*.

## Contents
<!-- toc -->

## `for` and `while` loops
Loops are the primary method to make the computer perform a set of actions
repeatedly without literally writing out the actions for each time you want them
repeated. Like `if` statements, `for` and `while` loops have associated scopes,
where actions inside the scopes are performed some number of times. The
difference between `for` and `while` essentially lies in how the number of times
is decided.

`for` loops specify that anything in the scope should be executed for some
prescribed number of times, and are generally written in the form of
```python
for iterator in range(50):
    # your code here
    ...
```
Here, `range(50)` can be thought of as a special kind of collection that refers
to all the `int`s from `0` up to (but, as per Python convention, not including)
`50`. The `for` statement at the top tells the computer to set the variable
`iterator` equal to the first object it finds in `range(50)`, do whatever it
finds in the following scope, and then do it all over again after setting
`iterator` equal to the next object in `range(50)`. The repetition ends when the
loop reaches the last object in `range(50)`.

And we can also perform loops over the values of other collections of other
types of objects as well, using the same general syntax:
```Python
for char in "hello world!":
    print(char)
    # prints out each character on separate lines, one-by-one

my_list = ["Alice", "Bob", "Charlie", "Deborah"]
for name in my_list:
    if name != "Deborah":
        print(name + " is cool!")
    else:
        print(name + " is not cool :(")

my_dict = {"a": 1, "b": 2, "c": 3, "d": 4}
# this iterates over all the keys and values of `my_dict` paired off in tuples
# these "items" can be "unpacked" into the variable names `key` and `value`:
for (key, value) in my_dict.items():
    if value > 2:
        print(key)
```

`while` loops, on the other hand, specify that anything in the scope should be
executed over and over, so long as a given boolean expression is `True`, and
look like this:
```python
while control:
    # your code here
    ...
```
The core idea of a `while` loop is the same as for a `for` loop, except that
instead of setting an iterator variable for each repetition, the computer will
compute the value of `control` (which is a boolean expression) and do whatever
is in the scope if `control` is `True`. Caution should be taken when using
`while` loops, however, since there's nothing in principle that will stop the
computer from repeating the loop an infinite number of times, as in the
following example:
```python
while True:
    # your code here -- this code will loop indefinitely!
    ...
```
So if you're not careful, your program could get stuck in an infinite loop! The
best way to avoid this is to make sure that there is some way for your control
expression to eventually evaluate to `False`, but failing that, another way to
stop a loop early -- both `for` and `while` loops -- is by using the `break`
keyword, as in
```python
count = 0
while True:
    count = count + 1
    if count > 999:
        break
    # your code here
    ...
```

When the computer encounters `break`, it will immediately halt execution of the
surrounding `for` or `while` loop, and continue on from the end of the loop's
scope. The above is a particularly simple example of its use (you could actually
just swap the `while` part out for `for k in range(999)`), but `break` is a
handy tool for more complex situations.

Notice that here, also, we deal with the idea of a block. As with `if`
statements on the previous page, when we enter a loop, we also enter a block of
code that is associated with whatever opens the block only if it is indented to
the correct level. But importantly, things on the inside of the block can
interact with other things on the outside, and vice-versa. A common pattern that
you might see (and use) is to have what's generally called an *accumulator* to
perform some kind of operation on the iterations of a loop. For example, we
might want to use a loop to calculate a sum of terms, or assemble a `list`:
```Python
# Calculate the sum of all positive inverse squares

N = 1000000 # should be really large to approximate infinity
my_sum = 0.0 # initialize the accumulator
for k in range(1, N + 1):
    my_sum = my_sum + 1.0 / k**2

# find the error compared to what it's supposed to be (pi^2 / 6):
from math import pi
correct = pi**2 / 6
print("off by " + str(100 * (my_sum - correct) / correct) + "%")
```

```Python
# Calculate the integer parts of the square roots of all positive integers less
#   than 1000
from math import sqrt

integer_roots = list() # accumulate using a list here
for square in range(1000):
    k = int(sqrt(square)) # integer part of the square root
    integer_roots.append(k)
```

---
<details>
<summary><strong>Check your understanding</strong></summary>

1. Write a piece of code that constructs a `list` of numbers where the number at
   the $k$-th index is equal to $k / 2$ if $k$ is even, and $3 k + 1$ is $k$ is
   odd. The first few items in this list should be:
   ```Python
   [0, 4, 1, 10, 2, 16, 3, ...]
   ```
   Make this `list` 1000 items long and make sure that every item in this
   `list` is an `int`!

   *Hint: to find the "parity" of a number (whether it's even or odd), you'll
   probably find the operation `k % 2` useful. Try it on a few test numbers and
   see if you can find the pattern.*

1. When encountering loops, it's often useful to know -- without actually
   running any code -- how many iterations a loop will execute in order to get
   an idea of how long a program will take. Without running the following piece
   of code, how many iterations of the loop will be executed?
   ```Python
   N = 100

   k = 1
   while k**3 <= N:
       print("k**3 =", k**3)
       k += 1
   ```
   What if we had `N = 1000` instead? `N = 5000`? `N = 80000`?
<details>
<summary>Answer</summary>

1. First look at the `k % 2` operation on a few test cases:
   ```Python
   0 % 2 # == 0
   1 % 2 # == 1
   2 % 2 # == 0
   3 % 2 # == 1
   ...
   394857 % 2 # == 1
   ```
   This is hardly a proof, but it looks like `k % 2` evaluates to `0` if `k` is
   even, and `1` if `k` is odd! Using this, here's a simple way to build this
   list:
   ```Python
   numbers = list() # initialize the list
   N_numbers = 1000 # length of the list

   for k in range(N_numbers): # loop over list indices
       if k % 2 == 1:
           numbers.append(3 * k + 1)
       elif k % 2 == 0:
           numbers.append(k // 2) # remember to use `//` for ineger division!
   ```
   Note that the above can be simplified to use `else` instead of `elif k % 2 ==
   0` since `k % 2` can only return either `0` or `1`.

1. This snippet of code is essentially counting the number of positive integer
   values of `k` such that `k**3` is less than or equal to `N` by looping
   through all such numbers, starting with one. Considering this, the number
   of loop iterations for general $N$ is equal to the maximum value of $k$ that
   satisfies this condition, $\lfloor \sqrt[3]{N} \rfloor$, where $\lfloor
   \,\cdot\, \rfloor$ is the floor function, e.g. $\lfloor 5.273 \rfloor = 5$.

   So then for the desired values of `N`, we have
   ```
   N = 100      => 4 iterations
   N = 1000     => 10 iterations
   N = 5000     => 17 iterations
   N = 80000    => 43 iterations
   ```
</details>
</details>

---

## `<container>` comprehension
Kind of like `if` statements, `for` statements also have an alternative,
conceptually similar use that was added on as syntactic sugar to make `list`
assembly more ergonomic. The term for this is *list comprehension*, and the
syntax goes like this:
```Python
my_list_comp = [expr for iterator_variable in iterable_object]
```
which results in a `list` with the results of evaluating `expr` for every value
of `iterator_variable` contained in `iterable_object`. An `if` clause acting on
a boolean expression can also be added to filter out values:
```Python
my_list_comp = [expr for iterator_variable in iterable_object if bool_expr]
# my_list_comp will only contain items for which bool_expr evaluated to `True`
# at the time of construction
```

For a concrete example, we can use this syntax to replace the second of the use
cases (the one assembling the `list`) at the end of the last section:
```Python
from math import sqrt

# Calculate the integer parts of the square roots of all positive integers less
#   than 1000
integer_roots = [int(sqrt(square)) for square in range(1000)]
```
This is much more concise -- we've taken everything that happens inside the loop
and collapsed it down into one line by telling Python to just shove it all into
a `list`!

Actually, this touches a bit on an important concept from a later page on
[functional programming][functional], but for now we'll ignore it and note that
we can also use this syntax for the other container types we talked about in
[Data storage and containers][containers], except for `tuple`s:
```Python
# set comprehension
my_set_comp = { # line returns here are mostly arbitrary; just for code style
    "hello " + name + "!"
    for name in ["Alice", "Bob", "Charlie", "Deborah"] if name != "Deborah"
}

# dict comprehension
my_dict_comp = {chr(k + 96): k for k in range(1, 27)}
# creates {'a': 1, 'b': 2, 'c': 3, ..., 'z': 26}
```

---
<details>
<summary><strong>Check your understanding</strong></summary>

Try rewriting the construction of the list from the last **Check your
understanding** as a list comprehension.
<details>
<summary>Answer</summary>

One way to write this is
```Python
N_numbers = 1000
numbers = [3 * k + 1 if k % 2 == 1 else k // 2 for k in range(N_numbers)]
```
</details>
</details>

---

[functional]: ../02_paradigms-and-data-structures/functional.md
[containers]: ../00_types-and-input-output/containers.md

