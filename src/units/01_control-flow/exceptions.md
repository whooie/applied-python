# Exceptions and error-handling

On the previous page, we mentioned at the end that programmers can write
docstrings and type annotations for their functions to document what kind of
constraints and limitations are expected or imposed on their inputs and outputs.
But of course, words can only go so far. How can we formally implement these in
our programs?

## Contents
<!-- toc -->

## `raise` and `Exception` types
The answer to this, formally, lies in finding a way to make a program error out
when it encounters something undesirable, in the same way that every program
will report a `ZeroDivisionError` when you try to divide by zero, or a `dict`
will complain with a `KeyError` when you try to access a key that it doesn't
have. In Python this behavior is called *raising* an *exception*, and comprises
a vital way to send out red flags when things don't go as planned.

Considering their purpose, exceptions can be raised in any context and
immediately break out of all of the control flow constructs unless handled
(which we'll talk about at the bottom of this page). To raise an exception
yourself, simply use the `raise` keyword:
```Python
raise <expr>
```
where `<expr>` must be an expression that evaluates to an object of type
`Exception`. So what is an `Exception`? At an essential level, an `Exception` is
a container object that carries around the detailed of an error that occurred,
and can be instantiated (created) with the signature `Exception(*data)`, where
`*data` is any information that should be included with the error itself --
usually a `str` describing why the `Exception` was `raise`d at a minimum.

Suppose we're writing a function that computes the mean of a given `list` of
numbers. A few problem cases that we might worry about for other people using
the function (and hence might want to raise an exception for) are:

1. The argument passed to the function is not a `list`;
1. The given `list` contains objects of non-numerical types;
1. The given `list` is empty.

One basic pattern we can use to ensure that the input `list` is actually what we
want is to just go through and test everything with `if` statements. Here, we
can introduce the `isinstance(...)` function, which is an invaluable tool for
checking the types of objects. In the end, the function might end up looking
something like this. Remember, the central aim in raising exceptions is to be
both specific and concise with the information you include with an `Exception`.
```Python
Number = (int, float, complex)

def mean(data: list[Number]) -> (float, complex):
    # check that the type of the input is correct
    if not isinstance(data, list):
        raise ValueError("data must be in the form of a list")

    # check that data has elements
    N = len(data)
    if N == 0:
        raise ValueError("data list cannot be empty")

    # check that all of data's elements are numerical types
    for x in data:
        if not isinstance(x, Number):
            raise ValueError("encountered a non-numerical value")

    # we've verified that data satisfies our conditions, so now we can compute
    # the mean
    acc = 0.0
    for x in data:
        acc += x # lhs += rhs is shorthand for lhs = lhs + rhs
    return acc / N
```

But wait a second! Didn't we say above that `KeyError` and `ZeroDivisionError`
objects can be `raise`d as well? And what's with the `ValueError`s that are
being raised in this example here? What's going on? At this point, we can hint a
bit at a topic we'll cover when we talk about [object-oriented
programming][oop]. The short of it is that these other errors are actually
specific kinds of `Exception`s, in the same way that cats and dogs are both
specific kinds of animals (but no cat is a dog and no dog is a cat). In fact,
Python offers other kinds of `Exception`s as well, which you can find
[here][built-in-exceptions]. Again, more on this later. Through this
functionality, we gain an additional degree of freedom in how errors can be
communicated: Instead of raising a generic `Exception`, we can raise a
`ValueError` instead to communicate specific that something is wrong with a
value(s) that the function encounters.

### Following tracebacks
So what happens when an `Exception` is `raise`d? As we noted before, an
(unhandled) `Exception` immediately halts the execution of a program, causing
the program itself to crash. But if you have access to a text interface for
running your program, you'll also see what's called a *traceback* printed out to
the screen. Let's try running our `mean(...)` function with an input that we
know will cause an error:
```Python
# <mean(...) is defined here>

mean("bad input") # the input isn't a list, so we'll get a ValueError
```

When you run this program for yourself, you should see something like this:
```
Traceback (most recent call last):
  File " ... ", line 25, in <module>
    mean("bad input")
  File " ... ", line 6, in mean
    raise ValueError("data must be in the form of a list")
ValueError: data must be in the form of a list
```
(here, I've replaced file paths with `...` -- you should see file paths for your
system pointing to the actual location of `mean.py`). This is called a
*traceback*, and it's an indispensable tool for debugging programs. When Python
runs a program, it keeps track of which line it's executing, what blocks of code
it's running in what functions it's calling, and where those functions are
defined. When it encounters an Exception, it prints this information to the
screen, tracing the path out to the source of the error. These tracebacks go to
arbitrary depth -- if our error-generating function call were buried deep in a
stack of many other function calls, Python would still find the source just
fine. For instance,
```Python
def foo1():
    foo2()

def foo2():
    foo3()

def foo3():
    foo4()

def foo4():
    foo5()

def foo5():
    mean("bad input")

foo1()
```
prints out
```
Traceback (most recent call last):
  File " ... ", line 40, in <module>
    foo1()
  File " ... ", line 26, in foo1
    foo2()
  File " ... ", line 29, in foo2
    foo3()
  File " ... ", line 32, in foo3
    foo4()
  File " ... ", line 35, in foo4
    foo5()
  File " ... ", line 38, in foo5
    mean("bad input")
  File " ... ", line 6, in mean
    raise ValueError("data must be in the form of a list")
ValueError: data must be in the form of a list
```

### Easy testing with `assert`
Another useful tool for debugging is the `assert` keyword, which provides a
quick-and-dirty way to test the value of boolean expressions and raise an
`AssertionError` (another kind of `Exception`) if the value of the expression is
`False`. `assert` statements follow the syntax:
```Python
assert <expr>[, <error_body> ]
```
where `<expr>` is the boolean expression, and `[, <error_msg> ]` denotes an
optional body to include with the `AssertionError`. There's actually nothing
special about this kind of exception, except for the more convenient way to
create them -- the above `assert` statement is equivalent to
```Python
if not <expr>:
    raise AssertionError(<error_body>)
```
Due to the ease with which they can be created, they're a very convenient tool
to use for quickly testing the values or types (with `isinstance(...)`) of
certain values when you're debugging. They can, of course, be used wherever you
like outside of a debugging context, but this practice is not recommended since
they can only yield `AssertionError`s, rather than the full breadth of all the
different kinds of `Exception`s that are available.

---

<details>
<summary><strong>Check your understanding</strong></summary>

On the last page, the function that you wrote for the last **Check your
understanding** (the one about traversing the tree) will obviously fail if
either the `tree` or `path` arguments are not a dictionary or list,
respectively. Add an initial check using `isinstance` to make sure those
arguments are the right types before proceeding. If either of those arguments
are not the right type, raise an exception either an `assert` or `raise`
statement.
<details>
<summary>Answer</summary>

The modified function (now with annotations and a docstring!) should look
something like this:
```Python
def tree_traverse(tree: dict[str, dict | ...], path: list[str]) -> dict | ...:
    """
    Traverses a given tree along the given path, starting from the root node and
    returns the value at the end of the path.

    Parameters
    ----------
    tree : dict[str, dict | ...]
        Nested dictionary structure, where keys are strings naming nodes in the
        tree, and values can be either other dictionaries (indicating a branch
        node) or anything else (indicating a leaf node).
    path : list[str]
        List of nodes naming a path through the tree. Node should be named
        relative to the root of the tree.

    Returns
    -------
    val : dict | ...
        The value found at the end of the given path. Can be either a sub-tree
        as another nested-dictionary structure or any other value if the path
        leads to a leaf.
    """
    if not (isinstance(tree, dict) and isinstance(path, list)):
        raise ValueError(
            "args 'tree' and 'path' must be a dictionary and list!"
        )

    # account for the empty-path case by doing nothing
    if len(path) == 0:
        return tree

    # if only one item in the path, just index the dict
    elif len(path) == 1:
        return tree[path[0]]

    # recursive call: the path is shortened by one to progress to the base case
    else:
        return tree_traverse(tree[path[0]], path[1:])
```
</details>
</details>

---

## Catching and expecting exceptions
So what is one to do when `Exception`s may arise? Unfortunately, there's nothing
to be done for an unexpected exception, but if you ever deal with a function or
operation that you think may raise an `Exception` -- in a word, *fallible* --
Python provides a framework for defining the set of actions that the program
should take in the case that an `Exception` is encountered in the form of a
`try ... except` block.

These blocks are essentially quite similar to `if ... else` blocks, except that
they operate on exceptions, rather than the values of boolean expressions. In
their most basic form, they look like this:
```Python
try:
    # some actions that could raise an Exception
except:
    # actions for the program to take if an Exception is encountered
```
And like an `if ... else` block, we can further expand this out into several
cases that are triggered only for certain kinds of `Exception`s, as well as
the case where *no* exception is encountered:
```Python
try:
    # ...
except ValueError:
    # executed only if a ValueError is encountered
except ZeroDivisionError:
    # executed only if a ZeroDivisionError is encountered
except AssertionError:
    # executed only if an AssertionError is encountered
except:
    # executed for any exception that doesn't match the above cases
    # (should be placed last)
else:
    # executed only if no exceptions are encountered
finally:
    # executed in all cases
```
In `except` clauses, the exception can also be bound to a temporary variable
using the `as` keyword:
```Python
try:
    # do some error-generating stuff here
except Exception as err: # temporarily binds the exception to the `err` variable
    # the exception can be handled here or re-raised so that it can be detected
    # and handled in some other context
    print(err)
    raise err
```

This last example, which re-raises a caught `Exception` in its `except` clause,
introduces another important idea, which is that of error propagation -- in
other words, how errors are passed through a series of nested contexts. This
pattern is useful to have if, for example, you'd like to handle a only a certain
kind of `Exception` in a context, but still want to allow others to handle the
other kinds. For example:
```Python
# this function may raise a ZeroDivisionError
def one_over_x2(x: float) -> float:
    return 1.0 / x**2

# we'll use this higher-order function to easily compute the values of a
# function over a given list of inputs
def apply_func(f: type(lambda float: float), x: list[float]) -> list[float]:
    y = list()
    for xval in x:
        try:
            # try to compute the value of the function and append it to `y`
            y.append(f(xval)).
        except ZeroDivisionError:
            # if it generates a ZeroDivisionError, perturb the input by a small
            # amount and append that instead
            y.append(f(xval + 1e-12))
        except Exception as err:
            # if it raises some other kind of Exception, re-raise it
            raise err
    return y

# this is the main function that should handle everything
def main():
    x = [-1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0]
    try:
        y = apply_func(one_over_x2, x)
    except Exception:
        print("encountered an unexpected exception!")
```

---

<details>
<summary><strong>Check your understanding</strong></summary>

Two possible cases of failure for our tree-traversing function arise when the
indexing operations result in an error -- either because the requested key (node
name) does not exist within a certain sub-tree or because the value the function
is attempting to index is not a dictionary, which can occur if the given path
extends past a leaf node. The latter case is covered by the `isinstance`
checking added in the last **Check your understanding**, but the first will
result in a `KeyError` that we should re-interpret to be more comprehensible to
someone else who wants to use this function.

Add in a `try ... except` block that will catch this `KeyError` if thrown, and
re-raise it with a custom error message that accurately describes the source of
the error.
<details>
<summary>Answer</summary>

Here's one way to go about this:
```Python
def tree_traverse(tree: dict[str, dict | ...], path: list[str]) -> dict | ...:
    """
    Traverses a given tree along the given path, starting from the root node and
    returns the value at the end of the path.

    Parameters
    ----------
    tree : dict[str, dict | ...]
        Nested dictionary structure, where keys are strings naming nodes in the
        tree, and values can be either other dictionaries (indicating a branch
        node) or anything else (indicating a leaf node).
    path : list[str]
        List of nodes naming a path through the tree. Node should be named
        relative to the root of the tree.

    Returns
    -------
    val : dict | ...
        The value found at the end of the given path. Can be either a sub-tree
        as another nested-dictionary structure or any other value if the path
        leads to a leaf.
    """
    if not (isinstance(tree, dict) and isinstance(path, list)):
        raise ValueError(
            "args 'tree' and 'path' must be a dictionary and list!"
        )

    try:
        # account for the empty-path case by doing nothing
        if len(path) == 0:
            return tree

        # if only one item in the path, just index the dict
        elif len(path) == 1:
            return tree[path[0]]

        # recursive call: the path is shortened by one to progress to the base
        # case
        else:
            return tree_traverse(tree[path[0]], path[1:])
    except KeyError:
        raise KeyError(
            "node '" + path[0] + "' does not exist along the given path"
        )
```
</details>
</details>

---

[oop]: ../02_paradigms-and-data-structures/object-oriented.md
[built-in-exceptions]: https://docs.python.org/3/library/exceptions.html

