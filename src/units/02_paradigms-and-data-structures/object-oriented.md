# Object-oriented programming

The first paradigm we'll talk about is *object-oriented* programming. As the
name suggests, this paradigm is centered around the manipulation of objects to
achieve computational goals. But what is an object, and why is it a useful
construct?

## Contents
<!-- toc -->

## Types revisited
Back in Unit 0, we laid out all of Python's built-in types, from numbers and
strings to lists and dictionaries. Each of these types can be instantiated in
some way, e.g. `5`, `8`, and `1` are all *instances* of the `int` type,
`["hello", "world"]` is an instance of the `list` type, and so on. Hopefully by
now it's become clear that any given *object* in a program has to have a type
and, depending on what that type is, can exhibit a wide array of different
behaviors and interactions with other objects.

But now let's look at this from the opposite direction: Suppose you're given an
object of unknown type -- call it `X`. In fact, imagine we're writing a function
called `add_five(X)`, and within the body of this function, we want to have the
operation `X + 5`. How can we (or Python, for that matter) know what to do when
interpreting this expression? In the case where `X` is a numeric type the
answer is obvious, but what if `X` were literally anything else? If `X` were a
list or a string Python would expect the `+` to indicate a concatenating
operation, and kicks back a `TypeError: can only concatenate list (not "int") to
list`, and if `X` were something else like a set, Python just says `TypeError:
unsupported operand type(s) for +: 'set' and 'int'`. So then a natural question
arises: What, from Python's point of view, makes an int different from a list?

### Types, generally
The fundamental question to answer here is: What is a type? To answer this in
the context of Python, let's first briefly talk about "vectors". What is a
vector? Well, the answer can vary depending on who you ask. To be a bit
reductive, a programmer might say a vector is just a series of objects of a
single type strung together under a common name. A physicist, on the other hand,
might say a vector is a quantity with a certain magnitude and direction (for
whatever "magnitude" and "direction" might mean in some context), that may or
may not be invariant under a given family of transformations. Further, a
mathematician might say a vector is just an element of a "vector space", which
is a set that is closed under addition and scalar multiplication.

All of these definitions differ profoundly -- not just in their details, but in
ways that significantly affect how the idea of what a "vector" is will come to
be understood and used in whatever problems they may play a role in. An
underlying theme, though, is the notion that a "vector" can be anything *as long
as it satisfies all the properties that a vector should have*. To put it simply,
a vector is -- generally -- anything that behaves like a vector, for whatever
one decides it means for something to "behave like a vector."

This is the essence of how Python approaches types. In fact it even has a name,
*duck typing*, and is usually captured by the phrase "if it walks like a duck
and quacks like a duck, then it's a duck". In other terms: if a object adds,
multiplies, subtracts, and divides like an `int`, then it's an `int`.

So then coming back to our `X + 5` example, the way Python knows what to do when
it encounters this expression is by first checking whether it knows of an
addition operation that can add `5` to whatever type `X` is. If there is such an
operation, Python will execute it, and if not, it'll raise a `TypeError`. In
other words, Python doesn't care at all what the exact type of object `X` is,
but only whether it is of a *class* of object to which `5` can be added.

And to finally bring us back to the question of what a type even is, then, we
can answer this from Python's point of view: A type is a category of object
having certain properties and behaviors that mark it as being distinct from
another category of object. On a technical level, a type in Python is a
category of object that is characterized by having a certain set of *attributes*
(associated data) and *methods* (associated functions).

## Classes
As you might expect by now, Python allows programmers to define their own type
classes. This is done through the use of the `class` keyword, like so:
```python
# define a custom class called `MyClass`
class MyClass:
    """
    Similar to functions, this syntax supports the use of docstrings.
    Try calling `help(MyClass)`!
    """
    ...
```

To keep things from getting too abstract in this chapter, we'll start a running
implementation of a class called `ThreeVector`, which will model the behavior of
a vector (in the usual geometric sense) in 3D space, and build on it as we
encounter more concepts. Let's start it off:
```python
class ThreeVector:
    """
    Defines a vector in 3D space.
    """
    # the `pass` keyword tells Python to ignore everything below it until the
    # end of the scope
    pass
```

Once defined, a class can be *instantiated* (that is, an instance of the class
can be created) by calling the name of the class like a function:
```python
my_vector = ThreeVector() # create a new `ThreeVector` object
# this verifies that `my_vector` is an instance of the `ThreeVector` class!
assert isinstance(my_vector, ThreeVector)
```

### Attributes and `self`
Okay, so now we have a class called `ThreeVector`, and can instantiate it. But
right now, it doesn't do anything important or hold any significant data. What
properties should a `ThreeVector` have? Since we're defining `ThreeVector`s as
being in 3D space, we should expect that each `ThreeVector` needs to have at
least three numbers to unambiguously identify any location. We can see this
readily in the three most common representations of vectors in $\mathbb{R}^3$,
which are Cartesian ($x$, $y$, $z$), cylindrical polar ($r$, $\pphi$, $z$), and
spherical polar ($r$, $\pphi$, $\theta$). Since it's conceivable that a
hypothetical user of this class might want to use any of these three, we should
also include some identifier for which representation a given `ThreeVector`
uses.

So now we have several pieces of information that we'd like to associate with
every `ThreeVector`: three numbers specifying a location in $\mathbb{R}^3$, and
one additional label identifying which representation the three numbers belong
to. These are what we'd call *attributes* of the object, and act rather like
variables that get carried around wherever the object goes. Like dictionaries,
we can assign values by just using the `=` operator, except here attributes
(and, as we'll discuss later, methods) are denoted using `.` notation:
```python
v = ThreeVector()
v.x = 5.0
v.y = 2.0
v.z = 10.0
v.repr = "cartesian"

# now this data is associated with `v`, and goes wherever it goes:
def print_vector(v: ThreeVector):
    print("ThreeVector:", v.x, v.y, v.z, v.repr)

print_vector(v) # ThreeVector: 5.0 2.0 10.0 cartesian
```

This gets the job done, but is seriously error-prone. For example, what if
`print_vector` is passed a `ThreeVector` that hasn't yet had these attributes
assigned? In that case, we get an `AttributeError` when `v.x` is referred to
inside the call to `print`:
```python
v = ThreeVector()
print_vector(v)
# AttributeError: 'ThreeVector' object has no attribute 'x'
```

The best way to guarantee that each and every `ThreeVector` that could ever be
created will have these attributes is by adding a *constructor method* to the
class, which is essentially a special function called `__init__` (we'll see
below that there are other "special" functions that use the `__` naming
convention) that defines extra steps to take when a class is instantiated. The
syntax looks like this:
```python
class ThreeVector:
    """
    Defines a vector in 3D space.
    """

    # default values can be defined here, just like in regular functions
    def __init__(self, x1: float, x2: float, x3: float, repr: str="cartesian"):
        # we can perform some extra processing on the inputs to `__init__`
        # just like in a regular function
        self.x1 = float(x1)
        self.x2 = float(x2)
        self.x3 = float(x3)
        if repr not in ["cartesian", "spherical", "cylindrical"]:
            raise Exception("invalid representation " + str(repr) + "!")
        self.repr = repr

# now we can assign values to attributes right when the object is instantiated
v = ThreeVector(5.0, 2.0, 10.0, "cartesian")

# redefine `print_vector` because we changed the attribute names
def print_vector(v: ThreeVector):
    print("ThreeVector:", v.x1, v.x2, v.x3, v.repr)

print_vector(v) # ThreeVector: 5.0 2.0 10.0 cartesian
```
We can immediately notice two things. First is that this function doesn't have a
`return` statement, even though we're supposed to be using it to create a new
value and assign it to the variable `P`. Second is the appearance of a function
argument called `self` that seems not to be assigned a value when the
constructor `ThreeVector(...)` is called.

Both are resolved upon learning that, when the `ThreeVector(...)` constructor is
called, Python doesn't directly call the `ThreeVector.__init__` method. After
`ThreeVector.__init__` is defined, the real process can be thought of more as a
call to a regular function called `ThreeVector` that goes something like:
```python
def ThreeVector(x1: float, x2: float, x3: float, repr: str) -> ThreeVector:
    # <instantiate a ThreeVector object and call it v>
    v.__init__(x1, x2, x3, repr) # calling a method using `.` automatically
                                 # passes the object as the first argument
    return v
```
To be clear, the real process is definitely not as straightforward as this. The
essence of this example is to show that `__init__` methods don't directly
instantiate objects -- that's taken care of invisibly by Python -- but only
modify the attributes of a newly created object. Additionally, when an object's
method is called using the `.` syntax, the object itself is passed to the `self`
argument of the method's definition, which explains its use in `__init__`'s
definition.

Once created using our new `__init__` constructor method, the attributes of a
`ThreeVector` can still be modified or referred to in the same way:
```python
v = ThreeVector(5.0, 2.0, 10.0, "cartesian")
print_vector(v) # ThreeVector: 5.0 2.0 10.0 cartesian

v.x1 = -8.0
print_vector(v) # ThreeVector: -8.0 2.0 10.0 cartesian
```
But crucially, the constructor is what allowed the attributes to be set
automatically, and in a way that guarantees they'll be present for all
`ThreeVector`s: Now there's no way to instantiate a `ThreeVector` that doesn't
involve setting these attributes, so we can guarantee that we'll never get an
`AttributeError` when attempting to refer to them!

### Methods
As we've alluded to already, methods are functions that are *bound* to
particular objects and follow a particular calling convention (where the object
itself is passed as the first argument of the function) that gives them access
to the object's encapsulated data. So let's extend our `ThreeVector` class with
a few methods that give it the behavior one would typically expect it to have.

We can first add a simple method to compute the magnitude (or, equivalently, the
distance from the origin) of a vector. Since a magnitude is a property of the
vector itself and doesn't depend on anything else, we should be able to call
this method with no arguments, like so:
```python
v = ThreeVector(3.0, 4.0, 0.0)
v.magnitude() # should return 5.0
```
One implementation that gives the behavior:
```python
from math import sqrt

class ThreeVector:
    # other definitions...

    def magnitude(self) -> float:
        if self.repr == "cartesian":
            return sqrt(self.x1**2 + self.x2**2 + self.x3**2)
        elif self.repr == "spherical":
            return self.x1
        elif self.repr == "cylindrical":
            return sqrt(self.x1**2 + self.x3**2)
        else:
            # this case should be unreachable due to checks in `__init__`, but
            # it's good practice to cover it anyway
            raise Exception("invalid representation " + str(self.repr) + "!")
```
Note that, even though we want `ThreeVector.magnitude` to take no arguments, we
still need to have the `self` argument in its definition!

Another useful method we can implement is one that can convert between
representations. There are two ways we can implement this -- one is to have the
method create a new `ThreeVector` with converted coordinates and the other is to
have it modify the existing `ThreeVector` in place. Here's an implementation of
the first.
```python
from __future__ import annotations # might need this, depending Python version
from math import sqrt, sin, cos, atan2, acos
from copy import copy

class ThreeVector:
    # other definitions...

    def convert_repr(self, repr: str) -> ThreeVector:
        # assume representations follow
        #   'cartesian' => (x, y, z)
        #   'spherical' => (r, phi, theta)
        #   'cylindrical' => (r, phi, z)
        # with phi being the azimuthal angle
        if repr == self.repr:
            return copy(self)
        if self.repr == "cartesian" and repr == "spherical":
            x1 = self.magnitude()
            x2 = atan2(self.x2, self.x1)
            x3 = atan2(self.x3, sqrt(self.x1**2 + self.x2**2))
        elif self.repr == "spherical" and repr == "cartesian":
            x1 = self.x1 * cos(self.x2) * sin(self.x3)
            x2 = self.x1 * sin(self.x2) * sin(self.x3)
            x3 = self.x1 * cos(self.x3)
        elif self.repr == "cartesian" and repr == "cylindrical":
            x1 = sqrt(self.x1**2 + self.x2**2)
            x2 = atan2(self.x2, self.x1)
            x3 = self.x3
        elif self.repr == "cylindrical" and repr == "cartesian":
            x1 = self.x1 * cos(self.x2)
            x2 = self.x1 * sin(self.x2)
            x3 = self.x3
        elif self.repr == "spherical" and repr == "cylindrical":
            z = self.x1 * cos(self.x3)
            x1 = sqrt(self.x1**2 - z**2)
            x2 = self.x2
            x3 = z
        elif self.repr == "cylindrical" and repr == "spherical":
            x1 = sqrt(self.x1**2 + self.x3**2)
            x2 = self.x2
            x3 = atan2(self.x3, self.x1)
        else:
            raise Exception(
                "invalid representation in conversion "
                + str(self.repr) + " => " + str(repr)
            )
        return ThreeVector(x1, x2, x3, repr)
```

---

<details>
<summary><strong>Check your understanding</strong></summary>

Modify `ThreeVector.convert_repr` to modify the `ThreeVector`'s coordinates in
place.

<details>
<summary>Answer</summary>

In-place modification of `self` can be accomplished by changing the final line
of the original implementation,
```python
class ThreeVector:
    # ...

    def convert_repr(self, repr: str) -> ThreeVector:
        # ...

        return ThreeVector(x1, x2, x3, repr)
```
to instead set the values of `self`'s attributes,
```python
class ThreeVector:
    #...

    def convert_repr_inplace(self, repr: str) -> ThreeVector:
        # ...

        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
        self.repr = repr
```
</details>
</details>

---

Continuing on, we can use `ThreeVector.convert_repr` to easily implement another
useful method, `ThreeVector.rel_to`, that computes `self` relative to another
`ThreeVector`. In other words, given two vectors `self` and `other` (say), it
computes a third vector `self - other`. It's a good thing we have `convert_repr`
implemented at this point, because the process for calculating the new vector is
much nicer when both vectors are in a Cartesian representation:
```python
class ThreeVector:
    # ...
    def rel_to(self, other: ThreeVector) -> ThreeVector:
        # first convert both vectors to Cartesian representation
        self_cart = self.convert_repr("cartesian")
        other_cart = other.convert_repr("cartesian")
        # compute new (Cartesian) components
        x1 = self_cart.x1 - other_cart.x1
        x2 = self_cart.x2 - other_cart.x2 
        x3 = self_cart.x3 - other_cart.x3
        # return the new vector in `self`'s original representation
        return ThreeVector(x1, x2, x3, "cartesian").convert_repr(self.repr)
```

Following this, we can also implement dot and cross products in the same
style:
```python
class ThreeVector:
    # ...

    def dot(self, other: ThreeVector) -> float:
        self_cart = self.convert_repr("cartesian")
        other_cart = other.convert_repr("cartesian")
        return (
            self_cart.x1 * other_cart.x1
            + self_cart.x2 * other_cart.x2
            + self_cart.x3 * other_cart.x3
        )

    def cross(self, other: ThreeVector) -> ThreeVector:
        self_cart = self.convert_repr("cartesian")
        other_cart = other.convert_repr("cartesian")
        x1 = self_cart.x2 * other_cart.x3 - self_cart.x3 * other_cart.x2
        x2 = self_cart.x3 * other_cart.x1 - self_cart.x1 * other_cart.x3
        x3 = self_cart.x1 * other_cart.x2 - self_cart.x2 * other_cart.x1
        return ThreeVector(x1, x2, x3, "cartesian").convert_repr(self.repr)
```

And, of course, we could go on. One of the most notable things we haven't yet
implemented, though, is vector addition, where we'd expect something like the
following to be possible:
```python
v = ThreeVector(1.0, 2.0, 3.0)
u = ThreeVector(4.0, 5.0, 6.0)

v + u # should be equal to `ThreeVector(5.0, 7.0, 9.0)`
```
Of course, it's possible to implement a `ThreeVector.add` method capturing this
behavior, but for some applications it could be important for this operation to
use Python's built-in `+` operator. If we tried performing addition between two
`ThreeVector`s right now, Python would kick back an error: `TypeError:
unsupported operand type(s) for +: 'ThreeVector' and 'ThreeVector'`, so it looks
like this is mainly a problem of giving the right definition.

### Magic methods
The definition we need to provide Python in order for it to allow addition in
this way is that for what's known as a *magic method*. Essentially magic methods
are methods one can define for any custom class that have specific signatures,
and are called automatically by the Python interpreter under specific
circumstances. All magic methods have names that look like `__XXX__` --
specifically, they are all some term surrounded by two underscores.

One example of a magic method is `__add__`, which is called (if defined)
whenever two objects are placed on either side of the `+` operator.
Specifically, you can think of the expression `X + Y` as being directly
translatable to `X.__add__(Y)`. (There are secondary processes that kick in if
`X` doesn't have a `__add__` method, but we'll leave those alone for now.)

There are, of course, other magic methods to implement subtraction,
multiplication, and division, but also others that define other behaviors like
what gets printed out when `print` is called on a particular object or how an
object can be iterated over using the `for y in Y` syntax.

So let's start with an implementation of the `__add__` method -- what kind of
behavior should it have? In math, vector-valued quantities are only addable (for
the vast majority of uses and definitions) to other vector-valued quantities and
produce more vector-valued quantities as a result, so it's a reasonable decision
expect our `ThreeVector.__add__` method to follow the same pattern: This
operation should only be valid between two `ThreeVector`s and return another
`ThreeVector`. Additionally, the formula for vector addition (which is
element-wise) works out to be much nicer in a Cartesian representation, so we
can yet again make use of `ThreeVector.convert_repr`:
```python
class ThreeVector:
    # ...

    def __add__(self, other: ThreeVector) -> ThreeVector:
        # check that `other` is a ThreeVector and raise a special exception
        # otherwise
        if isinstance(other, ThreeVector):
            self_cart = self.convert_repr("cartesian")
            other_cart = other.convert_repr("cartesian")
            x1 = self.x1 + other.x1
            x2 = self.x2 + other.x2
            x3 = self.x3 + other.x3
            return ThreeVector(x1, x2, x3, "cartesian").convert_repr(self.repr)
        else:
            raise NotImplementedError
```

Note that we had to make a choice for what representation the new `ThreeVector`
should have. Here, we call `ThreeVector.convert_repr(self.repr)` to return the
new `ThreeVector` in the same representation as `self` (the object placed on the
left-hand side of the `+` operator), but a different choice could make more
sense, depending on what `ThreeVector` could be used for.

Now that `ThreeVector.__add__` is defined, we can try it out:
```python
u = ThreeVector(1.0, 2.0, 3.0)
v = ThreeVector(4.0, 5.0, 6.0)

u + v # evaluates to ThreeVector(5.0, 7.0, 9.0)
```

One other useful thing we can do is define the `__eq__` and `__ne__` methods for
`ThreeVector`, which define the behavior of the `==` and `!=` operators between
a `ThreeVector` and any other object -- `==` should return a `bool` that is
`True` if the two are equal (in a certain sense that we are going to define!)
and `False` otherwise, and vice verse for `!=`. One way to implement this
behavior is to just be lazy and simply require that for two `ThreeVector`s to be
equal, they need to have identical values for their attributes:
```python
class ThreeVector:
    # ...

    def __eq__(self, other: ThreeVector) -> bool:
        if isinstance(other, ThreeVector):
            return (
                self.x1 == other.x1
                and self.x2 == other.x2
                and self.x3 == other.x3
                and self.repr == other.repr
            )
        else:
            return False

    # we can get `__ne__` essentially for free!
    def __ne__(self, other: ThreeVector) -> bool:
        return not (self == other)
```
But a more interesting and useful thing to do is make this equality independent
of what the representation for either `ThreeVector` is:
```python
class ThreeVector:
    # ...

    def __eq__(self, other: ThreeVector) -> bool:
        if isinstance(other, ThreeVector):
            self_cart = self.convert_repr("cartesian")
            other_cart = other.convert_repr("cartesian")
            return (
                self_cart.x1 == other_cart.x1
                and self_cart.x2 == other_cart.x2
                and self_cart.x3 == other_cart.x3
            )
        else:
            return False

    # we can get `__ne__` essentially for free!
    def __ne__(self, other: ThreeVector) -> bool:
        return not (self == other)
```
With this second definition for `__eq__`, it doesn't matter what representations
`self` and `other` are, because they both get converted to a Cartesian
representation before their components are compared. In addition to making this
class behave more like its pure mathematical counterpart, this definition is
much more appealing because it removes a degree of freedom the two
`ThreeVectors` being compares can have, and makes it impossible for a certain
kind of bug to pop up in a program -- namely, where the user forgets or loses
track of what representation one or both `ThreeVector`s have and cause a
comparison to return `False` when it should really return `True`.

Also, with `__eq__` now defined, we can now write things like
```python
u = ThreeVector(1.0, 2.0, 3.0)
v = ThreeVector(4.0, 5.0, 6.0)

# assert statements are nicer to read
assert u + v == ThreeVector(5.0, 7.0, 9.0)

# this works too!
assert u + v == ThreeVector(5.0, 7.0, 9.0).convert_repr("spherical")
```

One final thing we can implement here to wrap up a bit is the `__str__` magic
method, which defines the output of the `str(...)` typecast as well as the
output of the `print` function when applied to a `ThreeVector`. Essentially, the
only requirement for this method is that it return a `str` when called, but one
would usually expect that the content of the `str` correspond closely with the
data contained in the object. So for our `ThreeVector`, one possible
implementation goes like this:
```python
class ThreeVector:
    #...

    def __str__(self) -> str:
        return (
            "ThreeVector("
            + f"x1={self.x1},"
            + f" x2={self.x2},"
            + f" x3={self.x3},"
            + f" repr={self.repr}"
            + ")"
        )
```
(and note that in this implementation we've used a very useful tool for
formatting strings called [f-strings][fstrings]).

[fstrings]: https://realpython.com/python-f-strings/

---

<details>
<summary><strong>Check your understanding</strong></summary>

Implement the following magic methods for `ThreeVector` to complete the usual
set of mathematical operations that can be performed, following the usual
definitions for mathematical vectors:
- `__sub__(self, other)`: defines `self - other`
- `__mul__(self, other)`: defines `self * other`
- `__rmul__(self, other)`: defines `other * self`
- `__truediv__(self, other)`: defines `self / other`
- `__neg__(self)`: defines `-self`
- `__pos__(self)`: defines `+self`
- `__abs__(self)`: defines `abs(self)`

Note:
- Addition and subtraction should involve two `ThreeVectors`, happens
  element-wise in a Cartesian representation, and returns another `ThreeVector`.
- Multiplication should involve just one `ThreeVector` along with a scalar value
  -- take this to be an `int` or `float` -- that multiplies each element in a
  Cartesian representation and should be commutative, meaning that
  `self * other == other * self`. The `__rmul__` (reverse multiply, called for
  `other * self`) method has been included to ensure this.
- Division should also involve just one `ThreeVector` along with a scalar value,
  but is not defined in the backwards direction -- `other / self` doesn't make
  sense when `self` is vector-valued.
- `__neg__` and `__pos__` in this context should be the same as multiplying by
  `-1` and `1`, respectively.
- For vectors, the notation $|\vec{v}|$ (equivalent to `abs(v)` in Python) is
  usually taken to mean the magnitude of the vector.
</details>

---

To tie everything together, here's a list of magic methods, along with a syntax
example and short description for each one. There's a lot -- this list isn't
even comprehensive -- usually they're only implemented when they're needed so
don't feel like you need to implement every single one of them for every class
you make! Note that while the arguments of each method need to match the
descriptions below in order for Python to call them correctly, the return types
can be anything -- the types listed here are only those that are generally
expected as a weak convention. `...` indicates that there's no strong
expectation. The term `Self` is used as a stand-in for the name of the
hypothetical class for which these methods are being hypothetically implemented.
This list also references a few topics we haven't covered yet, but should act as
a good reference once we have.

<details>
<summary><strong>Table of magic methods</strong></summary>
<div style="align: center; width: 150%; margin: 1% -25%;">

| Method signature                          | Called for                         | Purpose                                  |
|:------------------------------------------|:-----------------------------------|:-----------------------------------------|
| `__init__(self)`                          | `Self(...)`                        | Object construction                      |
| `__del__(self)`                           | `del self`                         | Object deletion                          |
| `__eq__(self, x) -> bool`                 | `self == x`                        | Equality comparison                      |
| `__ne__(self, x) -> bool`                 | `self != x`                        | Non-equality comparison                  |
| `__lt__(self, x) -> bool`                 | `self < x`                         | Less-than comparison                     |
| `__gt__(self, x) -> bool`                 | `self > x`                         | Greater-than comparison                  |
| `__le__(self, x) -> bool`                 | `self <= x`                        | Less-than-or-equal comparison            |
| `__ge__(self, x) -> bool`                 | `self >= x`                        | Greater-than-or-equal comparison         |
| `__pos__(self) -> Self`                   | `+self`                            | Multiply by 1                            |
| `__neg__(self) -> Self`                   | `-self`                            | Multiply by -1                           |
| `__abs__(self) -> ...`                    | `abs(self)`                        | Absolute value                           |
| `__invert__(self) -> ...`                 | `~self`                            | Invert value                             |
| `__round__(self, ndigits=None) -> int`    | `round(self, ndigits)`             | Round to some precision                  |
| `__floor__(self) -> int`                  | `math.floor(self)`                 | Round down                               |
| `__ceil__(self) -> int`                   | `math.ceil(self)`                  | Round up                                 |
| `__trunc__(self) -> int`                  | `math.trunc(self)`                 | Round closer to zero                     |
| `__add__(self, x) -> ...`                 | `self + x`                         | Addition                                 |
| `__sub__(self, x) -> ...`                 | `self - x`                         | Subtraction                              |
| `__mul__(self, x) -> ...`                 | `self * x`                         | Multiplication                           |
| `__floordiv__(self, x) -> ...`            | `self // x`                        | Integer division                         |
| `__truediv__(self, x) -> ...`             | `self / x`                         | Floating point division                  |
| `__mod__(self, x) -> ...`                 | `self % x`                         | Modular reduction (remainder)            |
| `__divmod__(self, x) -> (..., ...)`       | `divmod(self, x)`                  | Integer division and remainder           |
| `__pow__(self, x, mod=None)`              | `pow(self, x, mod)`/`self ** x`    | Raise to a power, optional reduction     |
| `__lshift__(self, x) -> ...`              | `self << x`                        | Bit-shift left                           |
| `__rshift__(self, x) -> ...`              | `self >> x`                        | Bit-shift right                          |
| `__and__(self, x) -> ...`                 | `self & x`                         | Bit-wise AND                             |
| `__or__(self, x) -> ...`                  | <code>self &#124; x</code>         | Bit-wise OR                              |
| `__xor__(self, x) -> ...`                 | `self ^ x`                         | Bit-wise XOR                             |
| `__radd__(self, x) -> ...`                | `x + self`                         | Reverse addition                         |
| `__rsub__(self, x) -> ...`                | `x - self`                         | Reverse subtraction                      |
| `__rmul__(self, x) -> ...`                | `x * self`                         | Reverse multiplication                   |
| `__rfloordiv__(self, x) -> ...`           | `x // self`                        | Reverse integer division                 |
| `__rtruediv__(self, x) -> ...`            | `x / self`                         | Reverse floating point division          |
| `__rmod__(self, x) -> ...`                | `x % self`                         | Reverse modular reduction                |
| `__rdivmod__(self, x) -> (..., ...)`      | `divmod(x, self)`                  | Reverse integer division and remainder   |
| `__rpow__(self, x, mod=None)`             | `pow(x, self, mod)`/`self ** x`    | Reverse raise to a power                 |
| `__rlshift__(self, x) -> ...`             | `x << self`                        | Reverse bit-shift left                   |
| `__rrshift__(self, x) -> ...`             | `x >> self`                        | Reverse bit-shift right                  |
| `__rand__(self, x) -> ...`                | `x & self`                         | Reverse bit-wise AND                     |
| `__ror__(self, x) -> ...`                 | <code>x &#124; self</code>         | Reverse bit-wise OR                      |
| `__rxor__(self, x) -> ...`                | `x ^ self`                         | Reverse bit-wise OR                      |
| `__iadd__(self, x) -> ...`                | `self += x`                        | In-place addition                        |
| `__isub__(self, x) -> ...`                | `self -= x`                        | In-place subtraction                     |
| `__imul__(self, x) -> ...`                | `self *= x`                        | In-place multiplication                  |
| `__ifloordiv__(self, x) -> ...`           | `self //= x`                       | In-place integer division                |
| `__itruediv__(self, x) -> ...`            | `self /= x`                        | In-place floating point division         |
| `__imod__(self, x) -> ...`                | `self %= x`                        | In-place modular reduction               |
| `__ipow__(self, x, mod=None)`             | `self **= x`                       | Raise to a power in place                |
| `__ilshift__(self, x) -> ...`             | `self <<= x`                       | In-place bit-shift left                  |
| `__irshift__(self, x) -> ...`             | `self >>= x`                       | In-place bit-shift right                 |
| `__iand__(self, x) -> ...`                | `self &= x`                        | In-place bit-wise AND                    |
| `__ior__(self, x) -> ...`                 | <code>self &#124;= x</code>        | In-place bit-wise OR                     |
| `__ixor__(self, x) -> ...`                | `self ^= x`                        | In-place bit-wise XOR                    |
| `__int__(self) -> int`                    | `int(self)`                        | Integer conversion                       |
| `__float__(self) -> float`                | `float(self)`                      | Floating point conversion                |
| `__bool__(self) -> bool`                  | `bool(self)`                       | Boolean conversion                       |
| `__oct__(self) -> str`                    | `oct(self)`                        | Octal (base-8) representation            |
| `__hex__(self) -> str`                    | `hex(self)`                        | Hexadecimal (base-16) representation     |
| `__index__(self) -> int`                  | `list[self]`/`int(self)`/`...`     | Conversion to list index                 |
| `__str__(self) -> str`                    | `str(self)`/`print(self)`/`...`    | Conversion to string                     |
| `__repr__(self) -> str`                   | `self`                             | Value representation in iPython          |
| `__format__(self, format_spec='') -> str` | `"{}".format(self)`                | Format as string                         |
| `__hash__(self) -> int`                   | `hash(self)`                       | Compute hash values (for e.g. dict keys) |
| `__dir__(self) -> list[str]`              | `dir(self)`                        | List attributes                          |
| `__sizeof__(self) -> int`                 | `sys.getsizeof(self)`              | Compute the size in bytes                |
| `__getattr__(self, x) -> ...`             | `self.x`/`getattr(self, x)`        | Access an attribute                      |
| `__setattr__(self, x, y)`                 | `self.x = y`/`setattr(self, x, y)` | Set the value of an attribute            |
| `__delattr__(self, x)`                    | `del self.x`/`delattr(self, x)`    | Delete an attribute                      |
| `__len__(self) -> int`                    | `len(self)`                        | Compute length                           |
| `__getitem__(self, x) -> ...`             | `self[x]`                          | Subscript (like a list or dict)          |
| `__setitem__(self, x, y)`                 | `self[x] = y`                      | Set the value of a subscript             |
| `__iter__(self) -> iterator`              | `iter(self)`/`for x in self`       | Iterate over elements                    |
| `__reversed__(self) -> iterator`          | `reversed(self)`                   | Reverse-iterate over elements            |
| `__contains__(self, x) -> bool`           | `x in self`                        | Membership test                          |
| `__instancecheck__(self, x) -> bool`      | `isinstance(self, x)`              | Check that `self` is an instance of x    |
| `__subclasscheck__(self, x) -> bool`      | `issubclass(self, x)`              | Check that `self` is a subclass of x     |
| `__call__(self, ...) -> ...`              | `self(...)`                        | Call as a function                       |
| `__copy__(self) -> Self`                  | `copy.copy(self)`                  | Shallow copy                             |
| `__deepcopy__(self) -> Self`              | `copy.deepcopy(self)`              | Deep copy                                |

</div>
</details>

## Inheritance
*Inheritance* is a central (if not defining) feature of object-oriented
programming, whereby a class can *inherit* or *derive* some of its behavior from
another. The common, default analogy used for this mechanism is that of a child
inheriting traits from a parent (which itself inherits traits from its own
parent), and this manifests in some of the relevant terminology: a *child* class
inherits from a *parent* class, and two classes inheriting from the same parent
are, in a sense, siblings.

However, this characterization fails to capture a key feature of how parent and
child classes (more generally known as *super classes* and *subclasses*) are
usually treated, which is more like the various branches of a biological tree.
Take, for example, the human species, *Homo sapiens*. In biology, the full
scientific classification of this species goes like:

- Kingdom: Animalia
- Phylum: Chordata
- Class: Mammalia
- Order: Primates
- Suborder: Haplorhini
- Infraorder: Simiiformes
- Family: Hominidae
- Subfamily: Hominidae
- Tribe: Hominini
- Genus: *Homo*
- Species: *H. sapiens*

This particular combination classifications names a single species of life
found on Earth that satisfies certain sets of conditions that continually narrow
in breadth as one goes down the list, and broadly follows the same pattern that
a chain of inheriting classes will in object-oriented programming. But while
children (in the everyday sense) are *not* considered to be their parents,
members of *H. sapiens* are considered also to be of members of kingdom
Animalia, of phylum Chordata, of class Mammalia, and so on.

This also holds for one class that inherits another in programming -- let's
rephrase this in Python. Suppose we have three classes, `ClassA`, `ClassB`, and
`ClassC`, where `ClassB` inherits from `ClassA` and `ClassC` inherits from
`ClassB`. Now suppose we instantiate an object `C` using `ClassC`'s constructor,
and verify that it is, in fact, an instance of this class:
```python
C = ClassC()
assert isinstance(C, ClassC)
```
But we can also try this for `ClassA` and `ClassB`:
```python
C = ClassC()
assert isinstance(C, ClassC) # pass
assert isinstance(C, ClassB) # pass
assert isinstance(C, ClassA) # pass
```
The key point here -- and, indeed, of inheritance -- is that any instance of a
class *is also an instance of any class it inherits from*, just like how any *H.
sapiens* individual is also a hominid.

But how do we actually go about declaring subclasses in Python? To start, here's
the syntax for declaring a parent class:
```python
class Child(Parent):
    ...
```
Python also supports "multiple inheritance," where a class can inherit from more
than one parent class:
```python
class Child(Parent1, Parent2, ...):
    ...
```
but the use of this feature is generally discouraged, since it can lead to
what's known as [the diamond problem][diamond-problem], where it can be
ambiguous which of the class's parents a particular attribute or method will be
derived from.

[diamond-problem]: https://en.wikipedia.org/wiki/Multiple_inheritance#The_diamond_problem

So far, we've talked a lot about what inheritance means for how objects can be
classified, but we haven't actually said anything about what it means for
individual attributes and methods. There are two rules, and the first goes like
this: Suppose we have an object `x` that is an instance of a class `X`. If, when
some attribute or method `y` is accessed (say through dot notation `x.y`),
Python first looks though everything explicitly defined either through direct
assignment (e.g. `x.y = ...`) or through the `def` keyword in `X`'s definition.
If any of these exist, the data named by it is returned. If none exists,
however, Python then looks through all of `X`'s parent classes (in the same
fashion) for the same definition in the order they're listed. Second, any child
class can overwrite anything originally defined in a parent class. In this way,
it's actually rather like the scoping rules discussed [back in the last
chapter][scoping-rules], except the name resolution order is defined not through
code structure, but rather as a simple sequence decided by the programmer.

[scoping-rules]: ../01_control-flow/functions.md#scopes-and-mutability

Let's make this more concrete with a couple examples. First, a simple one using
our `Class*` from above:
```python
class ClassA:
    def foo(self):
        print("ClassA.foo")

class ClassB(ClassA):
    def bar(self):
        print("ClassB.bar")

    def foobar(self):
        print("ClassB.foobar")

class ClassC:
    def foo(self):
        print("ClassC.foo")

    def bar(self):
        print("ClassC.bar")

class ClassD(ClassA, ClassC):
    def foobar(self):
        print("ClassD.foobar")

# instantiate each class
a = ClassA()
b = ClassB()
c = ClassC()
d = ClassD()
```
And here is what gets printed out when we call each of the methods that have
been defined. Go through this table carefully, and try to go through the
inheritance rules yourself for each one!

| Object | `foo`            | `bar`            | `foobar`            |
|:-------|:-----------------|:-----------------|:--------------------|
| `a`    | `ClassA.foo`     | --               | --                  |
| `b`    | `ClassA.foo`     | `ClassB.bar`     | `ClassB.foobar`     |
| `c`    | `ClassC.foo`     | `ClassC.bar`     | --                  |
| `d`    | `ClassA.foo`     | `ClassB.bar`     | `ClassD.foobar`     |

## Why not use a dictionary?
At this point, you may be thinking that all of this object stuff sounds an awful
lot like dictionary management. After all, if objects are just collections of
uniquely named fields and methods -- which are themselves nothing more than
functions with a special calling convention with `self` -- then it seems like
the above `ThreeVector.dot()` method is pretty much the same as something like
this:
```python
# here's a constructor for a "ThreeVector" -- for simplicity, assume Cartesian
def new_threevector(x1: float, x2: float, x3: float) -> dict[str, float]:
    return { "x1": x1, "x2": x2, "x3": x3 }

# this function computes the dot product between two "ThreeVector"s
def dot(v1: dict[str, float], v2: dict[str, float]) -> float:
    return v1["x1"] * v2["x1"] + v1["x2"] * v2["x2"] + v1["x3"] * v2["x3"]

# test it out
u = new_threevector(1.0, 2.0, 3.0)
v = new_threevector(4.0, 5.0, 6.0)
dot(u, v) # evaluates to 32.0, as expected
```

