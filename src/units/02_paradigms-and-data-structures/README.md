# Unit 2: Paradigms and data structures

By now, we've gone though all the essential elements needed to construct pretty
much any Python program you could imagine. With knowledge of Python's basic
types and methods to direct a program's control flow, one really has everything
needed to jump right in and start creating some very useful things! Except
there's a bit of a catch, because while it is true that one can do anything with
numbers, strings, containers, functions, operators, and control flow, it
certainly won't be *easy* or *convenient*.

In many ways, this is where the fun really begins, because here is where we'll
introduce some key ideas that form the foundation of how different programs can
be classified into certain styles and, rather like different works of
literature, the structure of a piece of code can imply certain meanings and
ideas that go beyond the literal words and symbols that are written down. In
this unit, we'll deal with *paradigms* in programming, which can best be thought
of as idealized, archetypal forms that programs can take on in ways supported by
key features and constructs in the language. There are [many acknowledged
paradigms][paradigms] that a programming language can fit or be more strongly
suited to; Python is a "multi-paradigm" language, meaning that it can be used to
write programs that contain elements from multiple paradigms. In particular,
this unit will deal with just two that are often discussed, which are
*object-oriented* and *functional* programming, and introduce two final
constructs Python makes available to the programmer that enable this higher
level of expressiveness.

Contents:
- [Object-oriented programming](object-oriented.md)
- [Functional programming](functional.md)
- [Common (basic) data structures](data-structures.md)
- [Exercises](exercises.md)

[paradigms]: https://en.wikipedia.org/wiki/Programming_paradigm

