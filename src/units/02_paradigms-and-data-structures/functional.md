# Functional programming

## Contents
<!-- toc -->

## What exactly is `range`?

### `map` and `filter`

## Generators and generator expressions

### Lazy evaluation

## Generator functions and `yield`

### The `__iter__` magic method

