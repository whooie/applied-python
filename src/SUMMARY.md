# SUMMARY

- [Homepage](README.md)

---

- [Unit 0: Types and input/output](units/00_types-and-input-output/README.md)
    - [Python's basic types](units/00_types-and-input-output/types.md)
    - [Data storage and containers](units/00_types-and-input-output/containers.md)
    - [Program input/output](units/00_types-and-input-output/program-io.md)
    - [Exercises](units/00_types-and-input-output/exercises.md)

- [Unit 1: Control flow](units/01_control-flow/README.md)
    - [Boolean values and logic](units/01_control-flow/logic.md)
    - [Automated iteration](units/01_control-flow/loops.md)
    - [Functions](units/01_control-flow/functions.md)
    - [Exceptions and error-handling](units/01_control-flow/exceptions.md)
    - [Exercises](units/01_control-flow/exercises.md)

- [Unit 2: Paradigms and data structures](units/02_paradigms-and-data-structures/README.md)
    - [Object-oriented programming](units/02_paradigms-and-data-structures/object-oriented.md)
    - [Functional programming](units/02_paradigms-and-data-structures/functional.md)
    - [Common (basic) data structures](units/02_paradigms-and-data-structures/data-structures.md)
    - [Exercises](units/02_paradigms-and-data-structures/exercises.md)

- [Unit 3: Basic scripting patterns](units/03_basic-scripting-patterns/README.md)
    - [Program design](units/03_basic-scripting-patterns/program-design.md)
    - [Command-line arguments](units/03_basic-scripting-patterns/command-line-args.md)
    - [Configuration files](units/03_basic-scripting-patterns/config-files.md)

- [Unit 4: Common libraries](units/04_common-libraries/README.md)
    - [`numpy` and `scipy`](units/04_common-libraries/numpy-scipy.md)
    - [`pandas`](units/04_common-libraries/pandas.md)
    - [`lmfit`](units/04_common-libraries/lmfit.md)
    - [`matplotlib` and `plotly`](units/04_common-libraries/matplotlib-plotly.md)
    - [Exercises](units/04_common-libraries/exercises.md)

- [Unit 5: Basic calculus and statistics](units/05_basic-calculus-and-statistics/README.md)
    - [Derivatives](units/05_basic-calculus-and-statistics/derivatives.md)
    - [Integrals](units/05_basic-calculus-and-statistics/integrals.md)
    - [Discrete probability](units/05_basic-calculus-and-statistics/discrete-probability.md)
    - [Continuous probability](units/05_basic-calculus-and-statistics/continuous-probability.md)
    - [Quick-and-dirty regression](units/05_basic-calculus-and-statistics/regression.md)
    - [Exercises](units/05_basic-calculus-and-statistics/exercises.md)

- [Projects](projects/README.md)
    - [Newton's method](projects/newton.md)
    - [Mandelbrot visualization](projects/mandelbrot.md)
    - [Ising model memories](projects/ising.md)
    - [Markovian language prediction](projects/markov.md)

---

- [Resources](resources.md)

