# assume all numbers here are integers

from math import ceil, sqrt
from typing import Optional

def is_prime(n: int) -> Optional[int]:
    """
    Tests whether a number `n` is prime by checking for divisibility by all
    numbers from `2` to `ceil(sqrt(n))`. If a divisor `d` is found (i.e. `n` is
    not prime), the divisor is returned, otherwise `None`. This function will
    always find the smallest divisor if `n` is non-prime.

    Note that `d` is guaranteed to be prime: If `d` divides `n` and `d` has
    some divisor `k` < `d`, then `k` must divide `n` as well. Since the
    divisibility of `n` is checked starting from the smallest possible value and
    `k` < `d`, it would have already been found!

    Parameters
    ----------
    n : int
        The number whose prime-ness is to be tested.

    Returns
    -------
    d : Optional[int]
        The smallest divisor of `n` if `n` is not prime, otherwise `None`.
    """
    for d in range(2, ceil(sqrt(n)) + 1):
        if (n % d) == 0:
            return d
    return None

def prime_factors(n: int) -> list[int]:
    """
    Recursively computes all of `n`'s prime factors, which are returned in a
    list in ascending order.

    Parameters
    ----------
    n : int
        The number whose factors are to be found.

    Returns
    -------
    factors : list[int]
        `n`'s prime factors, listed in ascending order.
    """
    d = is_prime(n) # test whether `n` is prime
    if d is None:
        # base case: if `n` is prime, then its only prime divisor is itself.
        return [n]
    else:
        # recursive case: if `n` is not prime, then `d` can be divided out and
        # we can repeat the process for `n // d`, prepending `d` to whatever
        # other factors are found
        return [d] + prime_factors(n // d)

