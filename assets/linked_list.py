from __future__ import annotations
from typing import TypeVar, Iterator, Union, Any
import copy
import sys

_recursionlimit = sys.getrecursionlimit()
Self = TypeVar("Self")
T = TypeVar("T")

class LinkedList:
    def __init__(self, *items) -> None:
        if len(items) > 0:
            self.head = items[0] \
                    if isinstance(items[0], Node) else Node(items[0])
            self.tail = items[-1] \
                    if isinstance(items[-1], Node) else Node(items[-1])
            cur = self.head
            for item in items[1:]:
                cur.linkr(item if isinstance(item, Node) else Node(item))
                cur = cur.nx
            self.tail = cur
        else:
            self.head = None
            self.tail = None
        self.length = len(items)

    def copy(self) -> Self:
        return copy.copy(self)

    def deepcopy(self) -> Self:
        return copy.deepcopy(self)

    def clone(self) -> Self:
        return self.deepcopy()

    def iter(self) -> Iterator[Node[T]]:
        cur = self.head
        while cur is not None:
            yield cur
            cur = cur.nx

    def __iter__(self) -> Iterator[T]:
        return self.iterval()

    def iterval(self) -> Iterator[T]:
        cur = self.head
        while cur is not None:
            yield cur.data
            cur = cur.nx

    def reviter(self) -> Iterator[Node[T]]:
        cur = self.tail
        while cur is not None:
            yield cur
            cur = cur.pv

    def __reversed__(self) -> Iterator[T]:
        return self.reviter()

    def reviterval(self) -> Iterator[T]:
        cur = self.tail
        while cur is not None:
            yield cur.data
            cur = cur.pv

    def __len__(self) -> int:
        return self.length

    def __contains__(self, val: T) -> bool:
        for N in self:
            if N.data == val:
                return True
        return False

    def index(self, val: T) -> int:
        k = 0
        for N in self:
            if N.data == val:
                return k
            k += 1
        raise ValueError(f"{val} is not in self")

    def __add__(self, other: LinkedList[T]) -> Self:
        if not isinstance(other, LinkedList):
            raise NotImplementedError(
                f"expected LinkedList, found {type(other).__name__}")
        newself = self.clone()
        newother = other.clone()
        newself.tail.linkr(newother.head)
        newself.tail = newother.tail
        newself.length += newother.length
        return newself

    def move_into(self, other: LinkedList[T], idx: int):
        if not isinstance(other, LinkedList):
            raise TypeError(
                f"other must be type LinkedList, found {type(other).__name__}")
        other[idx].linkl(self.tail)
        other[idx] = self.head
        other.length += self.length
        self.head = None
        self.tail = None
        self.length = 0

    def move_onto(self, other: LinkedList[T]):
        if not isinstance(other, LinkedList):
            raise TypeError(
                f"other must be type LinkedList, found {type(other).__name__}")
        if other.tail:
            other.tail.linkr(self.head)
        else:
            other.tail = self.head
        if other.head is None:
            other.head = other.tail
        other.tail = self.tail
        other.length += self.length
        self.head = None
        self.tail = None
        self.length = 0

    def take_from(self, other: LinkedList[T], idx: int):
        if not isinstance(other, LinkedList):
            raise TypeError(
                "other must be type LinkedList, found {type(other).__name__}")
        self[idx].linkl(other.tail)
        self[idx] = other.head
        self.length += other.length
        other.head = None
        other.tail = None
        other.length = 0

    def take_onto(self, other: LinkedList[T]):
        if not isinstance(other, LinkedList):
            raise TypeError(
                f"other must be type LinkedList, found {type(other).__name__}")
        if self.tail:
            self.tail.linkr(other.head)
        else:
            self.tail = other.head
        if self.head is None:
            self.head = self.tail
        self.tail = other.tail
        self.length += other.length
        other.head = None
        other.tail = None
        other.length = 0

    def __mul__(self, n: int) -> Self:
        if not isinstance(n, int):
            raise NotImplementedError(
                f"expected integer, found {type(n).__name__}")
        newself = self.clone()
        for _ in range(n - 1):
            newself.take_onto(self.clone())
        return newself

    def __rmul__(self, n: int) -> Self:
        return self.__mul__(n)

    def _boundscheck(self, idx: int):
        if idx not in range(-self.length, self.length):
            raise IndexError(
                f"index {idx} is out of bounds for length {self.length}")

    def __getitem__(self, idx: int | slice) -> Union[T, LinkedList[T]]:
        if isinstance(idx, int):
            self._boundscheck(idx)
            if idx >= 0:
                return self.head._getitem_(idx)
            else:
                return self.tail._getitem_(idx)
        elif isinstance(idx, slice):
            newself = self.clone()
            step = 1 if idx.step is None else idx.step
            if step > 0:
                beg = 0 if idx.start is None \
                        else max(-self.length, idx.start)
                k = beg
                cur = newself[k]
                cur.unlinkl()
                end = self.length if idx.stop is None \
                        else min(self.length - 1, idx.stop)
                while k < end:
                    nx = cur._getitem_(step)
                    cur.linkr(nx)
                    k += step
                nx.unlinkr()
            elif step < 0:
                beg = -1 if idx.start is None \
                        else max(-self.length, idx.start)
                k = beg
                cur = newself[k]
                cur.unlinkr()
                end = -self.length if idx.stop is None \
                        else min(self.length - 1, idx.stop)
                while k >= end:
                    pv = cur._getitem_(step)
                    cur.linkl(pv)
                    k += step
                pv.unlinkl()
            else:
                raise ValueError("step cannot be zero")
            return newself
        else:
            raise ValueError(
                f"expected integer or slice, found {type(idx).__name__}")

    def get(self, idx: int, default: Any=None) -> Union[Node[T], Any]:
        if not isinstance(idx, int):
            raise ValueError(
                f"expected integer, found {type(other).__name__}")
        try:
            return self[idx]
        except:
            return default

    def getval(self, idx: int, default: Any=None) -> Union[T, Any]:
        res = self.get(idx)
        return res.data if isinstance(res, Node) else res

    def __setitem__(self, idx: int, val: Union[T, Node[T]]):
        if not isinstance(idx, int):
            raise TypeError(
                f"expected integer, found {type(other).__name__}")
        self._boundscheck(idx)
        n = val if isinstance(val, Node) else Node(val)
        n.unlink()
        if not (idx == 0 or idx == -self.length):
            n.linkl(self[idx - 1])
        if not (idx == self.length or idx == -1):
            n.linkr(self[idx + 1])

    def append(self, val: Union[T, Node[T]]):
        n = val if isinstance(val, Node) else Node(val)
        n.unlink()
        if self.tail:
            self.tail.linkr(n)
        self.tail = n
        if self.head is None:
            self.head = n
        self.length += 1

    def push(self, val: Union[T, Node[T]]):
        self.append(val)

    def prepend(self, val: Union[T, Node[T]]):
        n = val if isinstance(val, Node) else Node(val)
        n.unlink()
        if self.head:
            self.head.linkl(n)
        self.head = n
        if self.tail is None:
            self.tail = n
        self.length += 1

    def insert(self, idx: int, val: Union[T, Node[T]]):
        self._boundscheck(idx)
        n = val if isinstance(val, Node) else Node(val)
        n.unlink()
        if not (idx == 0 or idx == -self.length):
            n.linkl(self[idx - 1])
        if not (idx == self.length or idx == -1):
            n.linkr(self[idx])
        self.length += 1

    def pop(self, idx: int=-1) -> Node[T]:
        self._boundscheck(idx)
        n = self[idx]
        self.length -= 1
        return n.pinch()

    def popval(self, idx: int=-1) -> T:
        return self.pop(idx).data

    def __str__(self) -> str:
        return (
            "LinkedList("
            + ", ".join(str(n) for n in self)
            + ")"
        )

class Node:
    def __init__(self, data, nx: Node[T]=None, pv: Node[T]=None) -> None:
        self.data = data
        self.type = type(data)
        self.nx = nx
        self.pv = pv

    def _getitem_(self, k: int) -> Node[T]:
        if abs(k) > _recursionlimit:
            cur = self
            while not (k == 0 or k == -1):
                cur = cur.nx if k > 0 else cur.pv
                k -= k // abs(k)
            return cur
        else:
            if k == 0 or k == -1:
                return self
            elif k > 0:
                if self.nx:
                    return self.nx._getitem_(k - 1)
                else:
                    raise IndexError
            else:
                if self.pv:
                    return self.pv._getitem_(k + 1)
                else:
                    raise IndexError

    def linkl(self, other: Node[T]):
        if not isinstance(other, Node):
            raise TypeError(
                f"expected Node, found {type(other).__name__}")
        other.unlinkr()
        other.nx = self
        self.unlinkl()
        self.pv = other

    def linkr(self, other: Node[T]):
        if not isinstance(other, Node):
            raise TypeError(
                f"expected Node, found {type(other).__name__}")
        other.unlinkl()
        other.pv = self
        self.unlinkr()
        self.nx = other

    def link(self, pv: Node[T], nx: Node[T]):
        self.linkl(pv)
        self.linkr(nx)

    def unlinkl(self):
        if self.pv:
            self.pv.nx = None
        self.pv = None

    def unlinkr(self):
        if self.nx:
            self.nx.pv = None
        self.nx = None

    def unlink(self):
        self.unlinkl()
        self.unlinkr()

    def pinch(self) -> Self:
        if self.pv is None:
            self.unlinkr()
        elif self.nx is None:
            self.unlinkl()
        else:
            self.pv.linkr(self.nx)
        return self

    def __str__(self) -> str:
        return f"Node({self.data})"

